import pickle
import wx
from wx.lib.wordwrap import wordwrap
import os
import sys
import scripts.magic_svsi_configurator_gui as magic_svsi_configurator_gui
import webbrowser
import queue
import time
import datetime
import ipaddress
import logging
from license_base import LicenseBase
from collections import defaultdict
from ObjectListView import ObjectListView, ColumnDefn
from pydispatch import dispatcher
from scripts import (api_actions, datastore, firmware_actions, interface_monitor,
                     main_list_queue, preferences_menu, svsi_monitor, svsi_scan_actions,
                     svsi_scanner, web_actions)

# The MIT License (MIT)

# Copyright (c) 2019 Jim Maciejewski

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=int(os.environ.get('LOG_LEVEL', 50)),
    datefmt="%H:%M:%S",
    stream=sys.stderr)

logger = logging.getLogger("Magic_Svsi_Configurator")


class Magic_Svsi_Configurator_Frame(magic_svsi_configurator_gui.MagicSvsiConfiguratorFrame):
    def __init__(self, parent):
        magic_svsi_configurator_gui.MagicSvsiConfiguratorFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "msc.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "Magic Svsi Configurator"
        self.version = "v1.0.5"
        self.storage_path = os.path.expanduser(os.path.join('~', 'Documents', self.name))
        self.storage_file = "_".join(self.name.split()) + ".pkl"
        self.SetTitle(f'{self.name} {self.version}')
        self.prefs = self.load_config()
        os.makedirs(self.prefs.tmp_directory, exist_ok=True)

        self.main_list = ObjectListView(self.olv_panel, wx.ID_ANY,
                                        style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.main_list.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,
                            self.olv_panelOnContextMenu)
        self.main_list.Bind(wx.EVT_LIST_ITEM_ACTIVATED,
                            self.on_browser)
        self.main_list.Bind(wx.EVT_KEY_DOWN, self.on_key_down)

        self.columns = [ColumnDefn("Time", "right", 110, "arrival_time", stringConverter="%I:%M:%S%p"),
                        ColumnDefn("Name", "right", 110, "name"),
                        ColumnDefn("Type", "right", 80, "box_type"),
                        ColumnDefn("Model", "right", 110, "model"),
                        ColumnDefn("MAC", "right", 125, "mac_address"),
                        ColumnDefn("IP", "right", 120, "ip_address"),
                        ColumnDefn("Serial", "right", 150, "serial_number"),
                        ColumnDefn("Stream", "right", 80, "stream"),
                        ColumnDefn("Mode", "right", 110, "mode"),
                        ColumnDefn("Resolution", "right", 110, "resolution"),
                        ColumnDefn("Audio", "right", 80, "audio"),
                        ColumnDefn("Netlinx Enable", "right", 120, "netlinx_enable"),
                        ColumnDefn("Netlinx Status", "right", 120, "netlinx_status"),
                        ColumnDefn("Master Mode", "right", 120, "master_mode"),
                        ColumnDefn("Master IP", "right", 120, "netlinx_ip"),
                        ColumnDefn("Netlinx Device", 'right', 120, "netlinx_device"),
                        ColumnDefn("Netlinx System", "right", 120, "netlinx_system"),
                        ColumnDefn("Firmware Release", "right", 140, "firmware_release"),
                        ColumnDefn("Software Version", "right", 130, "software_version"),
                        ColumnDefn("Web Version", "right", 120, "web_version", stringConverter=self.convert_epoch_to_date),
                        ColumnDefn("LLDP Chassis ID", "right", 120, "lldp_chassis_id"),
                        ColumnDefn("LLDP SYS Name", "right", 120, "lldp_sys_name"),
                        ColumnDefn("LLDP Port ID", "right", 110, "lldp_port_id"),
                        ColumnDefn("LLDP Port Desc", "right", 120, "lldp_port_desc"),
                        ColumnDefn("HDCP", "right", 120, "hdcp"),
                        ColumnDefn("AES67 Enable", "right", 100, "aes67"),
                        ColumnDefn("AES67 Address", "right", 120, "aes67address"),
                        ColumnDefn("Status", "center", 220, "status")]

        self.set_selected_columns()
        self.main_list.SetEmptyListMsg("No discovered units")
        self.olv_sizer.Add(self.main_list, 1, wx.ALL | wx.EXPAND, 0)
        self.olv_sizer.Layout()
        # self.SetSize((self.main_list.GetBestSize()[0] + 40, 600))
        for item in self.prefs.main_list:
            item.status = ""
        self.main_list.AddObjects(self.prefs.main_list)
        self.units_upgrading = []

        dispatcher.connect(self.discovered_new_unit, signal="Unit Detected")
        dispatcher.connect(self.error_message, signal="Error Message")
        dispatcher.connect(self.interface_update, signal="Interface Update")
        dispatcher.connect(self.status_update, signal="Status Update")
        dispatcher.connect(self.http_progress, signal="HTTP Progress")
        dispatcher.connect(self.monitor_unit_for_reboot, signal="Unit Upgrading")
        dispatcher.connect(self.complete_upgrade, signal="Upgrade Complete")
        dispatcher.connect(self.remove_upgrading_unit, signal="Remove Upgrading Unit")

        self.multicast_listeners = []
        self.interface_list = []

        self.interface_queue = queue.Queue()
        ip_monitor = interface_monitor.InterfaceMonitor(self.interface_queue)
        ip_monitor.daemon = True
        ip_monitor.start()

        self.main_list_queue_thread = queue.Queue()
        update_queue = main_list_queue.MainListQueue(self.main_list, self.main_list_queue_thread)
        update_queue.daemon = True
        update_queue.start()

        self.web_queue = queue.Queue()
        for i in range(20):
            web_thread = web_actions.WebActionsThread(self.web_queue)
            web_thread.daemon = True
            web_thread.start()

        self.api_queue = queue.Queue()
        for i in range(20):
            api_thread = api_actions.APIActionsThread(self.api_queue, self)
            api_thread.daemon = True
            api_thread.start()

        self.scan_queue = queue.Queue()
        scan_thread = svsi_scan_actions.SVSIScanActionsThread(self.scan_queue)
        scan_thread.daemon = True
        scan_thread.start()

        self.license_thread = LicenseBase(name=self.name, version=self.version, message_callback=self.license_message)
        self.license_thread.daemon = True
        self.license_thread.start()

        self.firmware_queue = queue.Queue()
        # a single queue as every unit needs to download its own file
        firmware_thread = firmware_actions.FirmwareActionsThread(self.firmware_queue, self)
        firmware_thread.daemon = True
        firmware_thread.start()

        self.interface_queue.put(['send_update'])
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_refresh, self.redraw_timer)
        self.redraw_timer.Start(1000)

    def license_message(self, data):
        """We either request a serial number or show a message"""
        if "get_serial" in data:
            wx.CallAfter(self.license_get_serial, data['message'])
        if "show_message" in data:
            wx.CallAfter(self.license_show_message, data['message'])
        if "manual_activation" in data:
            wx.CallAfter(self.save_manual_activation, data['cipher_text'], data['serial'], data['stage_two'])
        if "complete" in data:
            self.Show()

    def save_manual_activation(self, cipher_text, serial, stage_two):
        self.Hide()
        if stage_two:
            # Stage two
            open_file_dialog = wx.FileDialog(self, message="Load Manual Activation",
                                             defaultDir=self.storage_path,
                                             defaultFile="",
                                             wildcard="Custom Files (*.lic;)|*.lic;",
                                             style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            if open_file_dialog.ShowModal() == wx.ID_OK:
                license_path = open_file_dialog.GetPath()
                success = self.license_thread.manual_activation(license_path, serial)
                if success:
                    self.Show()
                    return
                else:
                    dlg = wx.MessageDialog(parent=self,
                                           message=wordwrap("Unable to use that file for manual activation", 350, wx.ClientDC(self)),
                                           caption='Unlicensed',
                                           style=wx.OK)
                    dlg.ShowModal()

        dlg = wx.MessageDialog(parent=self,
                               message=wordwrap("Unable to contact server.\nWould you like to download the manual activation?", 350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            dlg = wx.FileDialog(self,
                                message='Save manual activation',
                                defaultDir=self.storage_path,
                                defaultFile=f"{serial}.txt",
                                wildcard="TXT files (*.txt)|*.txt",
                                style=wx.FD_SAVE)
            if dlg.ShowModal() == wx.ID_OK:
                file_path = dlg.GetPath()
                with open(file_path, 'w') as cipher_text_file:
                    cipher_text_file.write(cipher_text)
        wx.CallAfter(self.on_exit, None)

    def license_get_serial(self, message):
        self.Hide()
        dlg = wx.TextEntryDialog(parent=self,
                                 message=f"{message}\nPlease enter your serial number",
                                 caption=f'{self.name} Serial Number Required',
                                 style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            serial = dlg.GetValue()
            self.license_thread.verify_serial_with_server(serial)
            self.Show()
        else:
            self.license_show_message("This software requires a valid serial number to run.")

    def license_show_message(self, message: str, exit: bool = True):
        dlg = wx.MessageDialog(parent=self,
                               message=wordwrap(message, 350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK)
        dlg.ShowModal()
        if exit:
            wx.CallAfter(self.on_exit, None)

    def status_update(self, data):
        self.main_list_queue_thread.put(['refresh_obj', data])
        self.save_config()

    def restart_multicast(self):
        """We need a thread to listen on every interface"""
        if self.prefs.multicast_listen:
            for listener in self.multicast_listeners:
                # print('stopping multicast')
                listener.shutdown = True
                listener.join()
            for interface in self.interface_list:
                multicast_listener = svsi_monitor.SVSImonitor(ip_address=interface.ip_addresses[0])
                multicast_listener.daemon = True
                multicast_listener.start()
                self.multicast_listeners.append(multicast_listener)

    def on_refresh(self, event):
        self.interface_queue.put(['check_changes'])

    def on_preferences(self, event):
        """"""
        dia = preferences_menu.PreferencesConfig(self)
        dia.ShowModal()
        dia.Destroy()

    def on_key_down(self, event):
        """Grab Delete key presses"""
        key = event.GetKeyCode()
        if key == wx.WXK_DELETE:
            dlg = wx.MessageDialog(
                parent=self,
                message='Are you sure? \n\nThis will delete all selected items in the list',
                caption='Delete All Selected Items',
                style=wx.OK | wx.CANCEL)

            if dlg.ShowModal() == wx.ID_OK:
                self.on_delete(None)
                self.save_config()
            else:
                return
        event.Skip()

    def on_delete(self, _):
        """Deletes the selected item"""
        if len(self.main_list.GetSelectedObjects()) == len(self.main_list.GetObjects()):
            self.main_list.DeleteAllItems()
            self.save_config()
            return
        if len(self.main_list.GetSelectedObjects()) == 0:
            return
        self.main_list.RemoveObjects(self.main_list.GetSelectedObjects())
        self.save_config()

    def delete_all_items(self, _):
        """Deletes all items,selected or not"""
        dlg = wx.MessageDialog(parent=self, message='Are you sure? \n This will delete all items in the list',
                               caption='Delete All Items',
                               style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            self.main_list.DeleteAllItems()
            self.save_config()
        else:
            return

    def http_progress(self, data):
        for unit in self.main_list.GetObjects():
            if unit.ip_address == data['ip_address']:
                unit.status = data['message']
                dispatcher.send(signal='Status Update', data=unit)

    def monitor_unit_for_reboot(self, data):
        for unit in self.main_list.GetObjects():
            if unit.ip_address == data['ip_address']:
                unit.upgrade_status = "Waiting for reboot"
                self.api_queue.put(['monitor_unit_for_reboot', unit])
                return
        logging.critical('unable to match unit to ipaddress: ', data)

    def complete_upgrade(self, data):
        self.units_upgrading.remove(data)

    def remove_upgrading_unit(self, data):
        self.units_upgrading.remove(data)

    def convert_epoch_to_date(self, epoch):
        try:
            if epoch == "":
                my_date = ""
            else:
                my_date = time.strftime('%m/%d/%Y', time.localtime(int(epoch)))
        except Exception as error:
            print("unable to convert epoch to date: ", error)
            my_date = ""
        return my_date

    def on_get_status(self, event):
        for unit in self.main_list.GetSelectedObjects():
            self.api_queue.put(['get_status', unit])

    def on_get_netlinx(self, event):
        for unit in self.main_list.GetSelectedObjects():
            self.web_queue.put(['get_netlinx', unit, self.prefs])

    def on_set_netlinx(self, event):
        self.abort = False
        for unit in self.main_list.GetSelectedObjects():
            try:
                # try to set the device number
                self.prefs.last_netlinx_device = str(int(unit.netlinx_device))
            except Exception:
                logging.warning('unable to get device number')
            try:
                # check if the device number is valid
                str(int(self.prefs.last_netlinx_device))
            except Exception:
                self.prefs.last_netlinx_device = '0'
            if self.prefs.last_netlinx_device == '0':
                # auto config do not increment device_number
                dia = preferences_menu.NetlinxConfig(self, unit, int(self.prefs.last_netlinx_device))
            else:
                dia = preferences_menu.NetlinxConfig(self, unit, int(self.prefs.last_netlinx_device) + 1)
            dia.ShowModal()
            if self.abort:
                return

    def on_set_aes67(self, event):
        self.abort = False
        for unit in self.main_list.GetSelectedObjects():
            if unit.aes67 == '':
                self.error_message(data={'message': 'This unit is not reporting AES67 support. Please run get status first.',
                                         'caption': 'Unable to determine AES67, please get status first.'})
                return
            dia = preferences_menu.AES67Config(self, unit)
            dia.ShowModal()
            if self.abort:
                return

    def parse_firmware_folder(self, firmware_directory):
        """Goes through the firmware folder and returns the dictionary of files there"""

        firmware_dict = {}
        try:
            for (dir_path, dir_names, file_names) in os.walk(firmware_directory):
                for version_file in file_names:
                    if version_file.endswith(".ver"):

                        with open(os.path.join(dir_path, version_file), 'r') as f:
                            ver_file_txt = f.readlines()
                        ver_name = version_file[:-4]
                        firmware_dict[ver_name] = {}
                        include_list = []
                        for line in ver_file_txt:
                            if ':' in line:
                                key, value = line.split(':')
                            else:
                                continue
                            if key != "INCLUDE":
                                firmware_dict[ver_name][key] = value.strip()
                            else:
                                include_list.append(value.strip())
                        firmware_dict[ver_name]['INCLUDE'] = include_list
                        firmware_dict[ver_name]['path'] = os.path.join(dir_path)

        except Exception as error:
            logging.critical(f'Error parsing firmware folder: {error}')
        return firmware_dict

    def check_for_firmware_update_on_unit(self, unit, firmware_dict):
        status = {"status": "Firmware file not found", "firmware": [], "all_firmware": []}
        # Go through each firmware file
        for firmware in firmware_dict.keys():
            # Go through each INCLUDE item
            for item in firmware_dict[firmware]['INCLUDE']:
                if item in unit.serial_number:
                    # Check if upgrade is required
                    try:
                        u_ver = datetime.datetime.strptime(unit.software_version, '%m/%d/%Y')
                        f_ver = datetime.datetime.strptime(firmware_dict[firmware]['SWVER'], '%m/%d/%Y')
                    except Exception as error:
                        logging.critical(f'unable to parse firmware version: {error}')
                        status['status'] = "No update available"
                        return status
                    # print(unit.mac_address)
                    # print(f'Checking: unit {u_ver} < file {f_ver}')
                    # print(f'Is: ', u_ver < f_ver)
                    # Is unit version less than file version
                    # add to a new dict for all firmware
                    status['all_firmware'].append(firmware)
                    if u_ver < f_ver:
                        status['status'] = 'Update available'
                        status['firmware'].append(firmware)
                    else:
                        if status['status'] != 'Update available':
                            status['status'] = "No update available"
        return status

    def on_send_firmware(self, event):
        """Sends firmware to units"""
        # Check firmware folder and build dictionary of firmware files
        # {'N3KV2Update_2017-07-20': {'WEBVER': '1500526800', 'SWVER': '7/11/2017', 'MINSWVER': '07/21/2015', 'REL': '2.12.5', 'INCLUDE': ['N3122',
        firmware_dict = self.parse_firmware_folder(self.prefs.firmware_directory)

        # Need to put all units selected into sets by model
        # models: {'n2323': {'units': [unit, unit...],'firmware': firmware,
        #          'n2313': {'units': [unit, unit...], 'firmware': firmware
        models_dict = defaultdict(lambda: defaultdict(list))
        all_units_have_firmware_versions = True
        for unit in self.main_list.GetSelectedObjects():
            if not unit.software_version:
                all_units_have_firmware_versions = False
            models_dict[unit.serial_number[:5]]['units'].append(unit)
        if not all_units_have_firmware_versions:
            dlg = wx.MessageDialog(
                parent=self, message='Please get status on all units prior to updating firmware.',
                caption='Unable to determine current firmware version',
                style=wx.OK)
            dlg.ShowModal()
            return
        # print(models_dict)
        # verify which firmware to load to
        for model in models_dict.keys():
            status = self.check_for_firmware_update_on_unit(models_dict[model]['units'][0], firmware_dict)
            # if len(status['all_firmware']) > 1:
            # we need to pick which firmware we want to load
            dia = preferences_menu.GetFirmwareSelection(self, model, list(reversed(status['all_firmware'])))
            if dia.ShowModal() == wx.ID_OK:
                # print('selection: ', dia.firmware_files[dia.firmware_files_cmb.GetSelection()])
                models_dict[model]['firmware'] = dia.firmware_files[dia.firmware_files_cmb.GetSelection()]
                # print(dia.firmware_files)
                # print(dia.firmware_files_cmb.GetSelection())
                # print('result: ', models_dict[model]['firmware'])
                dia.Destroy()
            else:
                dia.Destroy()
                return
            # else:
            #     models_dict[model]['firmware'] = status['all_firmware'][0]
        # return
        # Get listen ips for units
        # ip_subnets [{'192.168.0.0/24': {'units': [unit, unit...], 'listen_ip': '192.168.0.200',
        #              '198.18.0.0/12':  {'units': [unit, unit...], 'listen_ip': '198.18.0.200'}]
        listener_dict = defaultdict(lambda: defaultdict(list))
        for unit in self.main_list.GetSelectedObjects():
            # if not unit.ip_address or not unit.nm:
            #     continue
            listener_dict[ipaddress.ip_network(f'{unit.ip_address}/{unit.netmask_address}', strict=False)]['units'].append(unit)

        # Attempt to get the listen_ip automatically
        for network in listener_dict.keys():
            for interface in self.interface_list:
                for i, address in enumerate(interface.ip_addresses):
                    if ipaddress.ip_network(f'{address}/{interface.subnet_addresses[i]}', strict=False) == network:
                        listener_dict[network]['listen_ip'] = address

        # Create a list of all ipv4 addresses
        all_local_ip_addresses = []
        for interface in self.interface_list:
            for address in interface.ip_addresses:
                if ipaddress.ip_address(address).version == 4:
                    all_local_ip_addresses.append(address)

        # Check if any networks still require listen_ip
        for network in listener_dict.keys():
            if 'listen_ip' not in listener_dict[network]:
                # print(f'we need to select a {network}')
                message = f'Which address should devices in the {str(network)} use to connect to this computer.'
                dia = preferences_menu.GetCommandIPInterface(self, message, all_local_ip_addresses)
                if dia.ShowModal() == wx.ID_OK:
                    listener_dict[network]['listen_ip'] = dia.interface_list[dia.interface_cmb.GetSelection()]
                    dia.Destroy()
                else:
                    dia.Destroy()
                    return
        # from pprint import pprint
        # pprint(listener_dict)
        # At this point we know what firmware to load and with ip address to each set of units
        # We have the file path, and just need to queue up the send
        # In the queue we need to do the following steps
        # 1. start HTTP
        # 2. command unit
        # 3. wait for download
        # 4. either add to list of upgrading units or timeout and send warning

        for model in models_dict.keys():
            for unit in models_dict[model]['units']:
                # print('unit: ', unit)
                # print('dict: ', listener_dict)
                for network in listener_dict.keys():

                    if unit in listener_dict[network]['units']:
                        # could there be multiple interfaces with the same network? I don't think windows allows this
                        # if so we could end up send the firmware twice, will break if we find one
                        # print('sending firmware to unit: ',
                        #       unit,
                        #       os.path.join(self.prefs.firmware_directory, models_dict[model]['firmware'] + '.bin'),
                        #       listener_dict[network]['listen_ip'])
                        unit.status = 'Unit queued for update'
                        dispatcher.send(signal='Status Update', data=unit)
                        # print('Updating: ', unit, os.path.join(self.prefs.firmware_directory, models_dict[model]['firmware'] + '.bin'), listener_dict[network]['listen_ip'])
                        firmware_name = models_dict[model]['firmware']
                        self.firmware_queue.put(['send_firmware_to_unit',
                                                 unit,
                                                 os.path.join(firmware_dict[firmware_name]['path'], firmware_name + '.bin'),
                                                 listener_dict[network]['listen_ip']])
                        unit.upgrade_status = "Sending Firmware"
                        self.units_upgrading.append(unit)
                        # [unit.ip_address] = {'firmware': firmware_dict[models_dict[model]['firmware']]['SWVER']}
                        self.api_queue.put(['monitor_unit_for_update', unit, firmware_dict[models_dict[model]['firmware']]['SWVER']])
                        break

        # In a polling thread
        # 1. poll upgrading units for upgrade if unreachable mark as rebooting and set verification flag
        # 2. poll verification units once reachable, attempt to confirm upgrade.
        # 3. update status, and remove from queue

    def on_auto_hdcp(self, event):
        for unit in self.main_list.GetSelectedObjects():
            if unit.box_type not in ["TX", "RX"]:
                self.error_message(data={'message': 'Unable to determine unit type',
                                         'caption': 'Unable to determine unit type, please get status first.'})
                return
            if unit.box_type == "TX":
                self.api_queue.put(['tx_hdcp', unit, 'on'])
                self.api_queue.put(['get_status', unit])
            if unit.box_type == "RX":
                self.api_queue.put(['rx_hdcp', unit, 'auto'])
                self.api_queue.put(['get_status', unit])

    def on_enable_hdcp(self, event):
        for unit in self.main_list.GetSelectedObjects():
            if unit.box_type not in ["TX", "RX"]:
                self.error_message(data={'message': 'Unable to determine unit type',
                                         'caption': 'Unable to determine unit type, please get status first.'})
                return
            if unit.box_type == "TX":
                self.api_queue.put(['tx_hdcp', unit, 'on'])
                # self.api_queue.put(['get_status', unit])
            if unit.box_type == "RX":
                self.api_queue.put(['rx_hdcp', unit, 'on'])
                # self.api_queue.put(['get_status', unit])

    def on_disable_hdcp(self, event):
        for unit in self.main_list.GetSelectedObjects():
            if unit.box_type not in ["TX", "RX"]:
                self.error_message(data={'message': 'Unable to determine unit type',
                                         'caption': 'Unable to determine unit type, please get status first.'})
                return
            if unit.box_type == "TX":
                self.api_queue.put(['tx_hdcp', unit, 'off'])
                # self.api_queue.put(['get_status', unit])
            if unit.box_type == "RX":
                self.api_queue.put(['rx_hdcp', unit, 'off'])
                # self.api_queue.put(['get_status', unit])

    def on_lldp(self, event):
        for unit in self.main_list.GetSelectedObjects():
            self.api_queue.put(['lldp', unit])

    def interface_update(self, sender, interface_list):
        """Processes updates"""
        if interface_list == self.interface_list:
            # No change
            return
        self.interface_list = interface_list
        self.restart_multicast()

    def error_message(self, data):
        dlg = wx.MessageDialog(parent=self,
                               message=data['message'],
                               caption=data['caption'],
                               style=wx.OK)
        dlg.ShowModal()

    def discovered_new_unit(self, sender, data, multicast):
        """Unit discovered by multicast"""
        if not self.prefs.multicast_listen and multicast:            # print('no listen')
            return
        self.main_list_queue_thread.put(['add_obj', data, multicast])
        self.save_config()

    def on_broadcast(self, event):
        my_interfaces = []
        for interface in self.interface_list:
            my_scan = svsi_scanner.SVSIScanner(interface=interface)
            my_interfaces.append(interface.description)
            my_scan.daemon = True
            my_scan.start()

        names = "\r".join(my_interfaces)
        dlg = wx.MessageDialog(parent=self,
                               message=f'Broadcast search has been sent via:\r\r{names}',
                               caption='Broadcast Sent',
                               style=wx.OK)
        dlg.ShowModal()

    def on_multicast(self, event):
        self.prefs.multicast_listen = self.multicast_chk.GetValue()
        if not self.prefs.multicast_listen:
            if self.multicast_listener is not None:
                # print('stopping multicast')
                self.multicast_listener.shutdown = True
                self.multicast_listener.join()
        else:
            self.restart_multicast()
        self.save_config()

    def on_add_by_ip(self, event):
        dia = preferences_menu.AddByIPConfig(self)
        dia.ShowModal()
        dia.Destroy()

    def on_manual_add(self, event):
        dia = preferences_menu.GetManualIP(self)
        dia.ShowModal()
        dia.Destroy()

    def on_txcontrol(self, event):
        for unit in self.main_list.GetSelectedObjects():
            if unit.box_type != 'TX':
                unit.status = "No TX on Decoders"
                dispatcher.send(signal='Status Update', data=unit)
                continue
            # Check what was clicked
            if event.GetEventObject().GetLabel(event.GetId()) == 'Enable TX':
                self.api_queue.put(['tx_control', unit, True])
            else:
                self.api_queue.put(['tx_control', unit, False])

    def on_aes67_enable(self, event):
        for unit in self.main_list.GetSelectedObjects():
            if unit.box_type != 'TX':
                unit.status = "No AES67 Enable on Decoders"
                dispatcher.send(signal='Status Update', data=unit)
                continue
            if event.GetEventObject().GetLabel(event.GetId()) == 'Enable AES67 Audio':
                self.api_queue.put(['aes67_audio', unit, True])
            else:
                self.api_queue.put(['aes67_audio', unit, False])

    def on_password_change(self, event):
        dia = preferences_menu.GetChangePassword(self)
        dia.ShowModal()
        if dia.proceed:
            password = dia.new_password
            current_password = dia.current_password
            for unit in self.main_list.GetSelectedObjects():
                self.web_queue.put(["set_password", unit, self.prefs, current_password, password])

    def on_api_command(self, event):
        dia = wx.TextEntryDialog(self,
                                 message="Please enter the API command you would like sent",
                                 caption="Send an API command")
        if dia.ShowModal() == wx.ID_OK:
            command = dia.GetValue()
        else:
            return
        for unit in self.main_list.GetSelectedObjects():
            self.api_queue.put(["send_command", unit, command])

    def on_browser(self, event):
        if self.check_for_none_selected():
            return

        for item in self.main_list.GetSelectedObjects():
            tmp_path = os.path.join(self.prefs.tmp_directory, f"{item.mac_address.replace(':', '')}.html")
            with open(tmp_path, "wb") as f:
                html = self.create_temp_html(item)
                f.write(html.encode())
            webbrowser.open(f'file:///{tmp_path}')

    def create_temp_html(self, unit):
        html = """<!DOCTYPE html>
<html>
<body>

<form action="##HTTP##://##IP_ADDRESS##/login.php" method="POST" name="auto_login">
  <input type="hidden" id="username" name="username" value="##USER##">
  <input type="hidden" id="password" name="password" value="##PASS##">
  <input type="hidden" id="Login" name="Login" value="Login">
</form>
</body>

<script>
window.onload = function(){
    document.forms['auto_login'].submit();
}
</script>
</html>"""
        if unit.https == "1":
            html = html.replace("##HTTP##", "https")
        else:
            html = html.replace("##HTTP##", "http")
        html = html.replace("##IP_ADDRESS##", unit.ip_address)
        html = html.replace("##USER##", self.prefs.svsi_username)
        html = html.replace("##PASS##", self.prefs.get_svsi_password())
        return html

    def on_add_localplay(self, event):
        if self.check_for_none_selected():
            return

        open_file_dialog = wx.FileDialog(self, message="Load Image File",
                                         defaultDir=self.storage_path,
                                         defaultFile="",
                                         wildcard="Custom Files (*.png;*.jpg;*.jpeg)|*.png;*jpg;*.jpeg",
                                         style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            image_path = open_file_dialog.GetPath()
            open_file_dialog.Destroy()
            # Get Scale Resolution
            self.abort = False
            self.host_play_image_path = image_path
            self.keepaspect = False
            self.scaleselect = "1280x720"
            dia = preferences_menu.GetScaleSettings(self)
            dia.ShowModal()
            dia.Destroy()
            if self.abort:
                return
            # Pass this on
            # print(self.abort, self.host_play_image_path, self.keepaspect, self.scaleselect)
            # return
            for unit in self.main_list.GetSelectedObjects():
                self.web_queue.put(['update_localplay_image', unit, self.prefs, (image_path, self.scaleselect, self.keepaspect)])

    def check_for_none_selected(self):
        """Checks if nothing is selected"""
        if len(self.main_list.GetSelectedObjects()) == 0:
            dlg = wx.MessageDialog(
                parent=self, message='Nothing selected...\nPlease click on the device you want to select',
                caption='Nothing Selected',
                style=wx.OK)
            dlg.ShowModal()
            return True

    def on_view_general(self, event):
        """Only show general columns"""
        self.prefs.cols_selected = ['Time', 'Name', 'Model', 'MAC', 'IP',
                                    'Serial', 'Stream', 'Mode', 'Resolution', 'Audio', 'AES67 Enable', 'AES67 Address', 'Status']
        self.set_selected_columns()
        for unit in self.main_list.GetObjects():
            self.api_queue.put(['get_status', unit])

    def on_view_netlinx(self, event):
        """Only show Netlinx columns"""
        self.prefs.cols_selected = ['Time', 'Name', 'Model', 'MAC', 'IP',
                                    'Netlinx Enable', 'Netlinx Status', 'Master Mode', 'Master IP', 'Netlinx Device', 'Netlinx System', 'Status']
        self.set_selected_columns()
        for unit in self.main_list.GetObjects():
            self.web_queue.put(['get_netlinx', unit, self.prefs])

    def on_view_lldp(self, event):
        """Only show lldp columns"""
        self.prefs.cols_selected = ['Time', 'Name', 'Model', 'MAC', 'IP',
                                    'LLDP Chassis ID', 'LLDP SYS Name', 'LLDP Port Desc', 'LLDP Port ID', 'Status']
        self.set_selected_columns()
        for unit in self.main_list.GetObjects():
            self.api_queue.put(['lldp', unit])

    def on_view_firmware(self, event):
        """Only show firmware columns"""

        self.prefs.cols_selected = ['Time', 'Name', 'Model', 'MAC', 'IP',
                                    'Firmware Release', 'Software Version', 'Web Version', 'Status']
        self.set_selected_columns()
        firmware_dict = self.parse_firmware_folder(self.prefs.firmware_directory)
        for unit in self.main_list.GetObjects():
            self.api_queue.put(['check_for_firmware_update', unit, firmware_dict])

    def set_selected_columns(self):
        """Sets the preferred columns"""
        cols_to_display = []
        for item in self.columns:
            if item.title in self.prefs.cols_selected:
                cols_to_display.append(item)
        self.main_list.SetColumns(cols_to_display)
        self.SetSize((self.main_list.GetBestSize()[0] + 40, 600))

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def update_required(self, sender, message, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def load_config(self):
        """Load Config"""
        try:
            with open(os.path.join(self.storage_path, self.storage_file), 'rb') as f:
                pick = pickle.load(f)
            return pick
        except Exception as error:
            logging.critical(f'unable to load plk: {error}')
            return datastore.Preferences()

    def save_config(self):
        """Update values in config file"""
        self.prefs.main_list = self.main_list.GetObjects()
        if not os.path.exists(self.storage_path):
            os.mkdir(self.storage_path)
        with open(os.path.join(self.storage_path, self.storage_file), "wb") as f:
            pickle.dump(self.prefs, f)

    def on_doc(self, event):
        """Show the documentation"""
        webbrowser.open(self.resource_path(os.path.join("doc", "Magic Svsi Configurator.pdf")))

    def on_about_box(self, _):
        """Show the About information"""

        description = """Magic Svsi Configurator is a tool for configuring Svsi decoders and encoders.
Features include setting and verifying Netlinx information, unit discovery by Broadcast, IP Search, and Multicast, and more."""

        license = """The MIT License (MIT)

Copyright (c) 2019 Jim Maciejewski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

        info = wx.adv.AboutDialogInfo()
        info.SetName(self.name)
        info.SetVersion(self.version)
        info.SetDescription(description)
        info.SetLicence(license)
        info.AddDeveloper('Jim Maciejewski')
        wx.adv.AboutBox(info)

    def on_close(self, event):

        for filename in os.listdir(self.prefs.tmp_directory):
            os.unlink(os.path.join(self.prefs.tmp_directory, filename))
        self.save_config()
        dispatcher.send(signal="Shutdown")
        event.Skip()

    def on_exit(self, event):
        dispatcher.send(signal="Shutdown")
        self.Close()


def main():
    """run the main program"""
    msc_app = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    _ = Magic_Svsi_Configurator_Frame(None)
    # msc_frame.Show()
    msc_app.MainLoop()


if __name__ == '__main__':
    main()
