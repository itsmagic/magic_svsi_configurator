import re

with open('n2222_response.txt', 'rb') as f:
    data = f.read()

# print(repr(data.decode()))

reg_pat = re.compile("(.*?):(.*?)(\\r\\n|$)")
my_dict = {}
for item in re.findall(reg_pat, data.decode()):
    my_dict[item[0]] = item[1]
print(my_dict)
