import os
from pydispatch import dispatcher
from threading import Thread
import http.server
import tempfile
import shutil


class SVSiHTTPDownloadTracker:
    sizeWritten = 0
    totalSize = 0
    lastShownPercent = 0

    def __init__(self, totalSize, client_ip):
        self.totalSize = totalSize
        self.client_ip = client_ip

    def handle(self, written):
        percentComplete = round((float(written) /
                                 float(self.totalSize)) * 100)

        if (self.lastShownPercent != percentComplete):
            self.lastShownPercent = percentComplete

            message = str(int(self.lastShownPercent)) + '%'
            dispatcher.send(signal='HTTP Progress', data={'ip_address': self.client_ip, 'message': message})
            if message == '100%' or message == '102%':  # not going to figure this out right now..
                dispatcher.send(signal='HTTP Progress', data={'ip_address': self.client_ip, 'message': 'Waiting for reboot'})
                dispatcher.send(signal='Unit Upgrading', data={'ip_address': self.client_ip})


class SVSiHTTPHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        """Serve a GET request."""
        content_length = os.path.getsize('temp.bin')
        tracker = SVSiHTTPDownloadTracker(content_length, self.client_address[0])

        self.send_response(200)
        self.send_header('Content-type', 'application/octet-stream')
        self.send_header('Accept-Ranges', 'bytes')
        self.send_header('Content-Length', content_length)
        self.send_header('Keep-Alive', 'timeout=5, max=100')
        self.send_header('Connection', 'Keep-Alive')
        self.end_headers()

        with open('temp.bin', 'rb') as f:
            # while self.wfile.write(f.read(1048576)):
            while self.wfile.write(f.read()):
                tracker.handle(f.tell())


class SVSIHTTPThread(Thread):
    def __init__(self, obj, bin_file='', server_address='0.0.0.0', server_port=50005):
        self.server_address = server_address
        self.obj = obj
        self.server_port = server_port
        self.shutdown = False

        # Need to make this 'self' so it isn't immediately deleted when we hit Init
        self.my_temp_dir = tempfile.TemporaryDirectory()

        shutil.copyfile(bin_file, os.path.join(self.my_temp_dir.name, 'temp.bin'))
        os.chdir(self.my_temp_dir.name)
        # print('bin: ', bin_file)
        # print('temp: ', self.my_temp_dir.name)
        dispatcher.connect(self.on_shutdown,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        Thread.__init__(self)

    def run(self):

        server_address = (self.server_address, self.server_port)

        self.httpd = http.server.HTTPServer(server_address, SVSiHTTPHandler)
        # self.httpd.timeout = 5
        self.httpd.serve_forever()
        message = 'HTTP Server stopped'

        # Change out of the temp directory so it can be cleaned up when the thread closes
        os.chdir(os.path.expanduser(os.path.join('~', 'Documents')))
        dispatcher.send(signal='HTTP Log', data=message)

    def on_shutdown(self, sender):
        self.shutdown = True
        os.chdir(os.path.expanduser(os.path.join('~', 'Documents')))
        self.httpd.shutdown()


def messages(sender, data):
    print(sender, data)


def main():
    dispatcher.connect(messages, signal='HTTP Log')
    dispatcher.connect(messages, signal='HTTP Port')
    dispatcher.connect(messages, signal='HTTP Progress')
    obj = {'my obj': 'my_obj'}
    test = SVSIHTTPThread(obj, os.path.expanduser(os.path.join('~', 'Documents', 'Magic Svsi Configurator', 'firmware', 'N3KV2Update_2017-07-20.bin')))
    test.setDaemon(True)
    test.start()
    # test.join()
    print('after start')
    import time
    time.sleep(22)
    print('sending shutdown')
    test.on_shutdown(None)
    # time.sleep(12)


if __name__ == '__main__':
    main()