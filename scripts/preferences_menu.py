from netaddr import IPRange, IPAddress
from pydispatch import dispatcher
import scripts.magic_svsi_configurator_gui as magic_svsi_configurator_gui
import os
import time
import wx
from magic_svsi_configurator import Magic_Svsi_Configurator_Frame
from scripts.datastore import SVSIUnit
from scripts.svsi_scanner import SVSIScanner


class PreferencesConfig(magic_svsi_configurator_gui.PreferencesMenu):
    """Sets the PreferencesMenu """

    def __init__(self, parent):
        magic_svsi_configurator_gui.PreferencesMenu.__init__(self, parent)

        self.parent = parent
        self.prefs = self.parent.prefs
        self.multicast_chk.SetValue(self.prefs.multicast_listen)
        self.svsi_username_txt.SetValue(self.prefs.svsi_username)
        self.svsi_password_txt.SetValue(self.prefs.get_svsi_password())
        self.dir_picker_txt.SetPath(self.prefs.firmware_directory)

        for item in self.prefs.cols_selected:
            column_name = "_".join(item.lower().split()) + '_chk'
            # print(column_name)
            getattr(self, column_name).SetValue(True)

    def on_firmware_directory(self, event):
        self.prefs.firmware_directory = self.dir_picker_txt.GetPath()

    def on_save(self, event):
        self.prefs.multicast_listen = self.multicast_chk.GetValue()
        self.prefs.svsi_username = self.svsi_username_txt.GetValue()
        self.prefs.set_svsi_password(self.svsi_password_txt.GetValue())
        self.prefs.cols_selected = []
        for item in self.parent.columns:
            if getattr(self, "_".join(item.title.lower().split()) + '_chk').GetValue():
                self.prefs.cols_selected.append(item.title)
        self.parent.set_selected_columns()
        self.parent.save_config()
        self.Destroy()

    def on_cancel(self, event):
        self.Destroy()


class NetlinxConfig(magic_svsi_configurator_gui.NetlinxSetup):
    """NetlinxConfig"""

    def __init__(self, parent, unit, device_number=None):
        super(NetlinxConfig, self).__init__(parent)
        self.parent = parent
        self.prefs = self.parent.prefs
        self.unit = unit
        if unit.name == "":
            self.SetTitle(f'Netlinx Configuration {self.unit.mac_address}')
        else:
            self.SetTitle(f'Netlinx Configuration {self.unit.name}')

        if self.unit.netlinx_enable == '':
            self.enable_chk.SetValue(True)
        else:
            self.enable_chk.SetValue(self.unit.get_netlinx_enable())

        if self.unit.netlinx_ip == '':
            self.netlinx_ip_txt.SetValue(self.prefs.last_master_ip)
        else:
            self.netlinx_ip_txt.SetValue(self.unit.netlinx_ip)
            self.prefs.last_master_ip = self.unit.netlinx_ip

        self.netlinx_port_txt.SetValue('1319')
        if device_number is None:
            self.device_number_txt.SetValue(self.unit.netlinx_device)
        else:
            self.device_number_txt.SetValue(str(device_number))
        if self.unit.netlinx_system == '':
            self.system_number_txt.SetValue('1')
        else:
            self.system_number_txt.SetValue(self.unit.netlinx_system)
        self.username_txt.SetValue(self.prefs.netlinx_username)
        self.password_txt.SetValue(self.prefs.get_password())

    def on_ok(self, event):
        # self.prefs.netlinx_enable = self.enable_chk.GetValue()
        # self.prefs.netlinx_ip = self.netlinx_ip_txt.GetValue()
        # self.prefs.netlinx_port = self.netlinx_port_txt.GetValue()
        # self.prefs.netlinx_device = self.device_number_txt.GetValue()
        # self.prefs.netlinx_system = self.system_number_txt.GetValue()
        self.prefs.last_master_ip = self.netlinx_ip_txt.GetValue()
        self.prefs.last_netlinx_device = self.device_number_txt.GetValue()
        self.prefs.netlinx_username = self.username_txt.GetValue()
        self.prefs.set_password(self.password_txt.GetValue())
        self.parent.save_config()
        data = {'mastermode': self.mode_cmb.GetStringSelection().lower(),
                'ipurl': self.netlinx_ip_txt.GetValue(),
                'port': self.netlinx_port_txt.GetValue(),
                'devicenumber': self.device_number_txt.GetValue(),
                'username': self.username_txt.GetValue(),
                'password': self.password_txt.GetValue(),
                'changenetlinx': 'Save'}
        if self.enable_chk.GetValue():
            data['enable'] = 'on'
        self.parent.web_queue.put(['set_netlinx', self.unit, data, self.prefs])
        self.Destroy()

    def on_cancel(self, event):
        self.Destroy()

    def on_abort(self, event):
        self.parent.abort = True
        self.Destroy()


class AES67Config(magic_svsi_configurator_gui.AES67Setup):
    """AES67Config"""

    def __init__(self, parent, unit):
        super(AES67Config, self).__init__(parent)
        self.parent = parent
        self.prefs = self.parent.prefs
        self.unit = unit
        if self.unit.box_type == 'TX':
            self.channels_static_txt.Hide()
            self.channels_cmb.Hide()
            self.Fit()

        if self.unit.name == "":
            self.SetTitle(f'AES67 Configuration {self.unit.mac_address}')
        else:
            self.SetTitle(f'AES67 Configuration {self.unit.name}')

        if self.unit.aes67 == '1':
            self.enable_chk.SetValue(True)
        else:
            self.enable_chk.SetValue(False)

        if self.unit.aes67address == '' or self.unit.aes67address == '0.0.0.0:5004':
            address, port = self.prefs.last_aes_67_address.split(':')
        else:
            address, port = self.unit.aes67address.split(':')
        self.aes67_address_txt.SetValue(address)
        self.aes67_port_txt.SetValue(port)

    def on_ok(self, event):

        self.prefs.last_aes_67_address = f"{self.aes67_address_txt.GetValue()}:{self.aes67_port_txt.GetValue()}"
        self.parent.save_config()

        data = {'enable': self.enable_chk.GetValue(),
                'address': self.aes67_address_txt.GetValue(),
                'port': self.aes67_port_txt.GetValue(),
                'channels': self.channels_cmb.GetStringSelection()}
        self.parent.api_queue.put(['aes67_control', self.unit, data])
        self.Destroy()

    def on_cancel(self, event):
        self.Destroy()

    def on_abort(self, event):
        self.parent.abort = True
        self.Destroy()


class AddByIPConfig(magic_svsi_configurator_gui.AddByIP):
    """NetlinxConfig"""

    def __init__(self, parent):
        super(AddByIPConfig, self).__init__(parent)

        self.parent = parent
        self.prefs = self.parent.prefs
        self.start_ip_txt.SetValue(self.prefs.start_ip)
        self.end_ip_txt.SetValue(self.prefs.end_ip)
        # Populate dropdown
        self.interface_cmb.Clear()

        self.scans_started = 0
        self.scans_completed = 0
        self.scans_done = False

        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_refresh, self.redraw_timer)

        for interface in self.parent.interface_list:
            dropdown = f'{interface.description}'
            self.interface_cmb.Append(dropdown)
        self.interface_cmb.SetSelection(0)
        self.Layout()
        self.Fit()

    def on_refresh(self, event):
        started = self.dlg.GetRange() - self.parent.scan_queue.qsize()
        cancelled, skipped = self.dlg.Update(started, f"{started} out of {self.dlg.GetRange()} scans started")
        if not cancelled:
            self.redraw_timer.Stop()
            self.dlg.Destroy()
            return
        if started == self.dlg.GetRange():
            self.redraw_timer.Stop()
            self.dlg.Destroy()
            return

    def on_range(self, event):
        if self.range_btn.GetValue():
            self.start_ip_name_txt.SetLabel('Start IP Address')
            self.end_ip_name_txt.Show()
            # self.end_ip_name_txt.SetValue('End IP Address')
            self.end_ip_txt.Show()
        else:
            self.start_ip_name_txt.SetLabel('IP Address')
            self.end_ip_name_txt.Hide()
            # self.end_ip_name_txt.SetValue('End IP Address')
            self.end_ip_txt.Hide()

    def on_send(self, event):
        # check ip addresses
        if self.range_btn.GetValue():
            # try:
            ip_range = IPRange(self.start_ip_txt.GetValue(), self.end_ip_txt.GetValue())
            # except Exception:
            #     dispatcher.send(signal="Error Message", data={"message": "Please check your IP Address as it appears invalid.", "caption": "Invalid IP address"})
            #     return
            self.prefs.start_ip = self.start_ip_txt.GetValue()
            self.prefs.end_ip = self.end_ip_txt.GetValue()
            self.parent.save_config()
            ip_address_to_scan = []
            for ip_address in ip_range:
                if ip_address.version == 4:
                    ip_address_to_scan.append(ip_address)
            self.scans_started = 0
            self.scans_completed = 0
            self.scans_done = False
            with self.parent.scan_queue.mutex:
                self.parent.scan_queue.queue.clear()
            dispatcher.send(signal="Shutdown Scan")
            maximum = len(ip_address_to_scan)
            self.dlg = wx.GenericProgressDialog("Discovering devices",
                                                f"{self.scans_started} out of {maximum} scans started",
                                                maximum=maximum,
                                                parent=self,
                                                style=wx.PD_CAN_ABORT | wx.PD_APP_MODAL | wx.PD_ELAPSED_TIME)

            interface = self.parent.interface_list[self.interface_cmb.GetSelection()]
            for ip_address in ip_address_to_scan:
                self.parent.scan_queue.put(['scan', interface, str(ip_address)])
            self.redraw_timer.Start(500)
            return
        else:
            try:
                my_ip = IPAddress(self.start_ip_txt.GetValue())
                if my_ip.version == 4:
                    self.prefs.start_ip = self.start_ip_txt.GetValue()
                    self.prefs.end_ip = self.start_ip_txt.GetValue()
                    self.parent.save_config()
                    interface = self.parent.interface_list[self.interface_cmb.GetSelection()]
                    test = SVSIScanner(interface, ip_address=str(my_ip))
                    test.setDaemon(True)
                    test.start()
                    self.Destroy()
                    return

            except Exception as error:
                print("Error in single scan: ", error)
        dispatcher.send(signal="Error Message", data={"message": "Please check your IP Address as it appears invalid.", "caption": "Invalid IP address"})

    def on_cancel(self, event):
        self.Destroy()


class GetChangePassword(magic_svsi_configurator_gui.PasswordChange):
    """Manual Add an IP"""

    def __init__(self, parent: Magic_Svsi_Configurator_Frame):
        super(GetChangePassword, self).__init__(parent)

        self.parent = parent
        self.proceed = False
        self.new_password = None
        lock = wx.Bitmap(self.parent.resource_path(os.path.join("icon", "lock.png")), wx.BITMAP_TYPE_ANY )
        self.show_passwords_btn.SetBitmap(lock)
        self.current_password = self.parent.prefs.get_svsi_password()
        self.current_password_txt.SetValue(self.current_password)
        self.current_password_plain_txt.SetValue(self.current_password)
        self.Fit()
        self.new_password_txt.SetFocus()

    def on_show_passwords(self, event):
        if self.current_password_txt.IsShown():
            self.current_password_txt.Hide()
            self.current_password_plain_txt.SetValue(self.current_password_txt.GetValue())
            self.current_password_plain_txt.Show()
        else:
            self.current_password_txt.Show()
            self.current_password_plain_txt.Hide()

        if self.new_password_txt.IsShown():
            self.new_password_txt.Hide()
            self.new_password_plain_txt.SetValue(self.new_password_txt.GetValue())
            self.new_password_plain_txt.Show()
        else:
            self.new_password_txt.Show()
            self.new_password_plain_txt.Hide()
        self.Layout()

    def on_current_password_plain(self, event):
        if self.current_password_plain_txt.IsShown():
            self.current_password_txt.SetValue(self.current_password_plain_txt.GetValue())

    def on_new_password_plain(self, event):
        if self.new_password_plain_txt.IsShown():
            self.new_password_txt.SetValue(self.new_password_plain_txt.GetValue())

    def on_check_password(self, event):
        new_pw = self.new_password_txt.GetValue()
        confirm_pw = self.confirm_password_txt.GetValue()
        if new_pw != confirm_pw or new_pw == "":
            if new_pw == "":
                self.confirm_password_txt.SetBackgroundColour(wx.NullColour)
            else:
                self.confirm_password_txt.SetBackgroundColour('yellow')
            self.confirm_password_txt.Refresh()
            return False
        else:
            self.confirm_password_txt.SetBackgroundColour('green')
            self.confirm_password_txt.Refresh()
            return True
        
    
    def on_ok(self, event):
        if self.on_check_password(None):
            self.current_password = self.current_password_txt.GetValue()
            self.new_password = self.confirm_password_txt.GetValue()
            self.proceed = True
            self.Destroy()

    def on_cancel(self, event):
        self.Destroy()


class GetManualIP(magic_svsi_configurator_gui.ManualIP):
    """Manual Add an IP"""

    def __init__(self, parent):
        super(GetManualIP, self).__init__(parent)

        self.parent = parent

    def on_add(self, event):
        device_ip = IPAddress(self.ip_address_txt.GetValue())
        if device_ip.version == 4:
            new_device = SVSIUnit(ip_address=str(device_ip))
            self.parent.api_queue.put(['get_status', new_device])
        self.Destroy()

    def on_cancel(self, event):
        self.Destroy()

class GetScaleSettings(magic_svsi_configurator_gui.LocalPlay):
    """NetlinxConfig"""

    def __init__(self, parent):
        super(GetScaleSettings, self).__init__(parent)

        self.parent = parent
        self.parent.abort = True
        self.file_name_txt.SetLabel(os.path.basename(self.parent.host_play_image_path))
        # Populate dropdown
        resolutions = ['720x480', '1024x768', '1280x720', '1280x800', '1400x1050', '1920x1080', '1920x1200']
        self.scale_cmb.Clear()
        for res in resolutions:
            self.scale_cmb.Append(res)
        self.scale_cmb.SetSelection(resolutions.index('1280x720'))

    def on_submit(self, event):
        self.parent.keepaspect = self.keep_aspect_chk.GetValue()
        self.parent.scaleselect = self.scale_cmb.GetValue()
        self.parent.abort = False
        self.Destroy()

    def on_cancel(self, event):
        self.Destroy()


class GetFirmwareSelection(magic_svsi_configurator_gui.Firmware):
    """Select from multiple firmware files"""

    def __init__(self, parent, model, firmware_files):
        super(GetFirmwareSelection, self).__init__(parent)

        self.parent = parent
        self.model = model
        self.firmware_files = firmware_files

        self.model_txt.SetValue(model)
        # Populate dropdown
        self.firmware_files_cmb.Clear()
        for firmware in self.firmware_files:
            dropdown = f'{firmware}'
            self.firmware_files_cmb.Append(dropdown)
            # self.firmware_files['selection'] = self.firmware_files[firmware]
        self.firmware_files_cmb.SetSelection(0)
        self.Fit()


class GetCommandIPInterface(magic_svsi_configurator_gui.CommandIPInterface):
    """Select from multiple firmware files"""

    def __init__(self, parent, message, interface_list):
        super(GetCommandIPInterface, self).__init__(parent)

        self.parent = parent
        self.message_txt.SetLabel(message)
        self.interface_list = interface_list
        # Populate dropdown
        self.interface_cmb.Clear()
        for interface in self.interface_list:
            dropdown = f'{interface}'
            self.interface_cmb.Append(dropdown)
        self.interface_cmb.SetSelection(0)
        self.Layout()
        self.Fit()
