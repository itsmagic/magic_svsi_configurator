import http.server
import socketserver
import os
from pydispatch import dispatcher
from threading import Thread


class SVSiHTTPDownloadTracker:
    sizeWritten = 0
    totalSize = 0
    lastShownPercent = 0

    def __init__(self,  totalSize, client_ip):
        self.totalSize = totalSize
        self.client_ip = client_ip

    def handle(self):
        self.sizeWritten += 16*1024
        percentComplete = round((float(self.sizeWritten) /
                                 float(self.totalSize)) * 100)

        if (self.lastShownPercent != percentComplete):
            self.lastShownPercent = percentComplete

            message = str(int(self.lastShownPercent)) + '%'
            dispatcher.send(signal='HTTP Progress', sender=(self.client_ip, message))
            if message == '100%' or message == '102%':  # not going to figure this out right now..
                dispatcher.send(signal='HTTP Progress', sender=(self.client_ip, 'Rebooting'))


class SimpleHTTPRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        """Serve a GET request."""
        f = self.send_head()
        if f:
            self.copyfile(f, self.wfile)
            f.close()
        else:
            pass

    def copyfile(self, source, outputfile):
        # self.copyfileobj(source, outputfile)
        length = 16*1024
        tracker = SVSiHTTPDownloadTracker(os.path.getsize(source.name), self.client_address[0])
        copied = 0
        while True:
            buf = source.read(length)
            if not buf:
                break
            outputfile.write(buf)
            copied += len(buf)
            tracker.handle()

    def log_message(self, format, *args):

        # print 'got a log 2'
        message = ("%s - - [%s] %s\n" %
                   (self.client_address[0],
                    self.log_date_time_string(),
                    format % args))
        dispatcher.send(signal='HTTP Log', sender=message)


class FirmwareThread(Thread):

    def __init__(self, file_path):
        Thread.__init__(self)
        self.shutdown = False
        self.file_path = file_path
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        server_address = ('', 5005)
        # my_request_handler = SimpleHTTPRequestHandler(self.file_path)
        httpd = http.server.HTTPServer(server_address, SimpleHTTPRequestHandler)
        while not self.shutdown:
            httpd.handle_request()

def messages(sender):
    print(sender)


def main():
    dispatcher.connect(messages, signal='HTTP Log')
    dispatcher.connect(messages, signal='HTTP Port')
    dispatcher.connect(messages, signal='HTTP Progress')
    obj = {'my obj': 'my_obj'}
    test = FirmwareThread(os.path.join('.', 'test.csv'))
    test.start()

if __name__ == '__main__':
    main()