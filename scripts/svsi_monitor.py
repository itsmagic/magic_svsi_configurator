import socket
import struct
import time
import datetime
from threading import Thread
from pydispatch import dispatcher
try:
    from scripts import datastore
except Exception:
    try:
        import datastore
    except Exception:
        pass
    pass


class SVSImonitor(Thread):
    """The SVSI thread"""

    def __init__(self, ip_address):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.ip_address = ip_address
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        Thread.__init__(self)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        """Run Worker Thread."""
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            port = 50019
            multicast_address = "239.254.12.16"
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((self.ip_address, port))
            # print('socket open')
            # group = socket.inet_aton("239.254.12.16")
            # mreq = struct.pack('4sL', group, socket.INADDR_ANY)
            # sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(self.ip_address))
            # print('joining: ', self.ip_address)

            sock.setblocking(0)
            # print(dir(sock))
            # print(sock.getsockname())

        except IOError as error:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            if error.args[0] == 10049:
                print("Skipping this error: ", error)
            else:
                print('Opening socket error: ', error)
                # Maybe we should warn the user?
                # dispatcher.send(signal="Error Message", sender=self, data={'message': 'Another program is using the SVSI multicast port.\rPlease check if N-Able is running,\ror use broadcast to discover units.', 'caption': 'Socket in use'})
            return
        data = ""
        while not self.shutdown:
            try:
                # print('waiting')
                data, source = sock.recvfrom(1024)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    time.sleep(1)
                    continue
                else:
                    print("Error listening: ", error)
            # check if it is a discovery message
            try:
                if data[:14].decode() != 'SVSI-Discovery':
                    print('not equal: ', data[:14].decode())
                    continue
                mac_address = ':'.join(['%02x' % item for item in data[16:22]])
                unit = datastore.SVSIUnit(arrival_time=datetime.datetime.now(),
                                          serial_number=data[30:44].decode(),
                                          name=data[50:67].decode(),
                                          mac_address=mac_address,
                                          ip_address=source[0],
                                          raw_multicast=data)
                # print(unit)
            except Exception as error:
                print('Error parsing packet: ', error)
                continue
            dispatcher.send(signal="Unit Detected", sender=self, data=unit, multicast=True)
        try:
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(self.ip_address))
            # print('leave: ', self.ip_address)
            sock.close()
            # print('socket closed')
        except Exception as error:
            print('Unable to close socket: ', error)


def incoming(data):
    print(data)


def new_unit(data):
    print(data)


def main():

    dispatcher.connect(incoming, signal="Error Message")
    dispatcher.connect(new_unit, signal="Unit Detected")
    test = SVSImonitor()
    test.setDaemon(True)
    test.start()
    import time
    time.sleep(30)
    test.shutdown = True
    # test.join()


if __name__ == '__main__':
    main()
