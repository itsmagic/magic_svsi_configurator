# check the output of netsh and make sure the multicast is on the desired interface
# script to parse netsh 

import subprocess

joins = subprocess.run(['netsh', 'interface', 'ip', 'show', 'joins'], capture_output=True)
# print(joins)
if joins.returncode == 0:
    for interface in joins.stdout.decode().split('\nInterface '):
        if '239.254.12.16' in interface:
            print(interface.split(':')[0])

