from threading import Thread
import requests
import time
import re
import os
import logging
from bs4 import BeautifulSoup
from pydispatch import dispatcher
from queue import Queue
from pprint import pprint

logger = logging.getLogger("WebActionsThread")

class WebActionsThread(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.shutdown = False
        self.protocol = "http"
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def shutdown_signal(self, sender):
        """Shut down the thread"""
        self.shutdown = True

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def login(self, ip_address, username, password):
        s = requests.Session()
        try:
            req = s.get(f"{self.protocol}://{ip_address}/login.php", timeout=10, verify=False)
        except requests.ConnectionError as error:
            print("Connection error: ", error)
            return
        self.protocol = req.url.split(':')[0]
        # print('protocol: ', self.protocol)
        data = {'username': username, 'password': password, 'Login': 'Login'}
        my_url = f"{self.protocol}://{ip_address}/login.php"
        req = s.post(my_url, data=data, verify=False)
        if req.status_code == 200:
            return s
        else:
            print(req.status_code, req.text)
            return None

    def get_hostlocal_play_values(self, job):
        obj = job[1]
        prefs = job[2]
        # login
        obj.status = 'logging in'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            session = self.login(ip_address=obj.ip_address, username=prefs.svsi_username, password=prefs.get_svsi_password())
        except Exception as error:
            obj.status = 'Connection error logging in' + str(error)
            dispatcher.send(signal='Status Update', data=obj)
            return
        if session is None:
            # print('failed to login')
            obj.status = 'failed to login'
            dispatcher.send(signal='Status Update', data=obj)
            return
        # Get host/localplay page
        obj.status = 'Getting supported values'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            req = session.get(f"{self.protocol}://{obj.ip_address}/localplay.php", verify=False)
        except Exception as error:
            obj.status = f'Unable to connect to unit: ' + str(error)
            dispatcher.send(signal='Status Update', data=obj)
            return
        # scale select
        try:
            soup = BeautifulSoup(req.text, features="html.parser")
            # print(soup.find(id="scaleselect"))
            pattern = re.compile("""<option data-height=\"(?P<dataheight>.*?)\" data-rawfile=\"(?P<rawfile>.*?)\" data-width=\"(?P<datawidth>.*?)\"(?P<selected> selected=\"selected\")? value=\"(?P<value>.*?)\">.*?</option>""")
            for match in pattern.finditer(soup.find(id="scaleselect")):
                if 'selected' in match:
                    obj.selected_scale = match['value']
                obj.playlistmodes.append({'dataheight': match['dataheight', 'datawidth': match['datawidth'], 'rawfile': match['rawfile'], 'value': match['value']]})
        except Exception as error:
            obj.status = f'Unable to get playlist modes, using defaults' + str(error)
            dispatcher.send(signal='Status Update', data=obj)
            # Use default values
            obj.playlistmodes = [{'dataheight': '1080', 'datawidth': '1920', 'rawfile': '0,44,148,638,1920,5,36,1080,4,62,116,10,41,1,9,191,0,0,0,74250', 'value': '1920x1080'},
                                 {'dataheight': '1200', 'datawidth': '1920', 'rawfile': '0,32,80,48,1920,6,26,1200,3,31,255,25,208,4,9,187,0,0,1,154000', 'value': '1920x1200'},
                                 {'dataheight': '800', 'datawidth': '1280', 'rawfile': '0,136,200,64,1280,3,24,800,2,31,255,25,61,3,9,191,0,0,1,82998', 'value': '1280x800'},
                                 {'dataheight': '720', 'datawidth': '1280', 'rawfile': '0,40,220,440,1280,5,20,720,5,62,116,10,41,1,9,191,0,0,0,74250', 'value': '1280x720'},
                                 {'dataheight': '1050', 'datawidth': '1400', 'rawfile': '0,128,208,16,1400,3,36,1050,1,31,255,25,123,4,9,191,0,0,1,114750', 'value': '1400x1050'},
                                 {'dataheight': '768', 'datawidth': '1024', 'rawfile': '0,136,160,24,1024,6,29,768,3,31,255,25,136,2,9,191,0,0,0,64804', 'value': '1024x768'},
                                 {'dataheight': '480', 'datawidth': '720', 'rawfile': '0,62,60,16,720,6,30,480,9,31,255,25,56,4,9,187,4,0,0,27000', 'value': '720x480'}]
        # file types
        try:
            # check it again to populate default values
            soup = BeautifulSoup(req.text, features="html.parser")
            pattern = re.compile("""image/(?P<filetype>.*?),""")
            for match in pattern.finditer(soup.find(id="newimageaudiofilebutton")):
                obj.file_types.append(match['filetype'])
        except Exception as error:
            obj.status = f'Unable to get file types, using defaults' + str(error)
            dispatcher.send(signal='Status Update', data=obj)
            # Use default file types
            obj.file_types = ['png', 'jpg', 'jpeg']
        obj.status = 'Host/Localplay values updated'
        dispatcher.send(signal='Status Update', data=obj)

    def update_localplay_image(self, job):
        obj = job[1]
        prefs = job[2]
        image_path, scaleselect, keepaspect = job[3]

        # login
        obj.status = 'logging in'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            session = self.login(ip_address=obj.ip_address, username=prefs.svsi_username, password=prefs.get_svsi_password())
        except Exception as error:
            obj.status = 'Connection error logging in' + str(error)
            dispatcher.send(signal='Status Update', data=obj)
            return
        if session is None:
            # print('failed to login')
            obj.status = 'failed to login'
            dispatcher.send(signal='Status Update', data=obj)
            return

        # Set raw / bin
        self.svsi_file_type = 'raw'
        # Check for duplicates
        if not (self.check_for_duplicate_image_names(obj, session, os.path.basename(image_path), scaleselect=scaleselect) and
                self.add_image_to_localplay(obj, session, image_path, scaleselect=scaleselect, keepaspect=keepaspect) and
                self.check_conversion_status(obj, session, os.path.basename(image_path), scaleselect=scaleselect) and
                self.assign_image_to_playlist(obj, session, os.path.basename(image_path), scaleselect=scaleselect)):
            return
        obj.status = 'Success'
        dispatcher.send(signal='Status Update', data=obj)

    def check_for_duplicate_image_names(self, obj, session, image_name, scaleselect='1920x1080'):
        checking_for_dups = True
        count = 0
        while checking_for_dups and count < 30:
            count += 1
            try:
                req = session.get(f"{self.protocol}://{obj.ip_address}/localplay.php", verify=False)
            except Exception as error:
                obj.status = f'Unable to connect to unit: ' + str(error)
                dispatcher.send(signal='Status Update', data=obj)
                return False
            # print(req.text)
            pattern = re.compile("""\<option title=(?P<name>\".*\")\s*data-width=(?P<width>\".*\")\s*data-height=(?P<height>\".*\")\s*data-filename=(?P<filename>\".*\")\s*value=(?P<value>\".*\")""")
            for match in pattern.finditer(req.text):
                # print(match['value'])
                if 'bin.jpg' in match['value']:
                    # print('this device uses bin')
                    self.svsi_file_type = 'bin'
                if 'raw.jpg' in match['value']:
                    # print('this device uses raw')
                    self.svsi_file_type = 'raw'
                if 'h264.jpg' in match['value']:
                    # print('this device uses h264')
                    self.svsi_file_type = 'h264'

                if f"LP_{image_name}_{scaleselect}.{self.svsi_file_type}.jpg" in match['value']:
                    # delete duplicate
                    data = {'DeleteLPFile': 'DeleteLPFile', 'file0': f"LP_{image_name}_{scaleselect}.{self.svsi_file_type}"}
                    r = session.post(f'{self.protocol}://{obj.ip_address}/localplay.php', data=data, verify=False)
                    if r.status_code == 200:
                        obj.status = f'Found duplicate, deleting it'
                    else:
                        obj.status = f'Unable to delete file. Error {r.status_code}'
                    dispatcher.send(signal='Status Update', data=obj)
                    time.sleep(1)
                    continue
            obj.status = 'No duplicates found'
            dispatcher.send(signal='Status Update', data=obj)
            checking_for_dups = False
        return True

    def add_image_to_localplay(self, obj, session, image_path, scaleselect='1920x1080', keepaspect='false'):
        try:
            files = {'newimageaudiofileArray[]': (os.path.basename(image_path), open(image_path, 'rb'))}
            data = {'keepaspect': keepaspect, 'fillchoose': '#000000', 'scaleselect': scaleselect, 'executesave': 'executesave'}
            try:
                r = session.post(f'{self.protocol}://{obj.ip_address}/localplay.php', data=data, files=files, verify=False)
            except Exception as error:
                obj.status = f'Unable to connect to unit: ' + str(error)
                dispatcher.send(signal='Status Update', data=obj)
                return False
            if r.status_code == 200:
                obj.status = f'Adding new file'
                dispatcher.send(signal='Status Update', data=obj)
            else:
                obj.status = f'Unable add file. Error {r.status_code}'
                dispatcher.send(signal='Status Update', data=obj)
                return False
        except Exception as error:
            obj.status = "Unable to add file. " + str(error)
            dispatcher.send(signal='Status Update', data=obj)
            return False
        return True

    def check_conversion_status(self, obj, session, image_name, scaleselect='1920x1080'):
        waiting = True
        count = 0
        conversion_started = False
        while waiting and count < 160:
            count += 1
            try:
                r = session.get(f"{self.protocol}://{obj.ip_address}/localplay.php", verify=False)
            except Exception as error:
                obj.status = f'Unable to connect to unit: ' + str(error)
                dispatcher.send(signal='Status Update', data=obj)
                return False
            if r.status_code != 200:
                obj.status = f'Unable check status. Error {r.status_code}'
                dispatcher.send(signal='Status Update', data=obj)
                return False
            if 'Image conversion in progress...' in r.text:
                conversion_started = True
            pattern = re.compile("""\<option title=(?P<name>\".*\")\s*data-width=(?P<width>\".*\")\s*data-height=(?P<height>\".*\")\s*data-filename=(?P<filename>\".*\")\s*value=(?P<value>\".*\")""")
            obj.status = f'Converting, elapsed seconds: {count}'
            dispatcher.send(signal='Status Update', data=obj)
            for match in pattern.finditer(r.text):
                # print(match['value'])
                if f"LP_{image_name}_{scaleselect}.{self.svsi_file_type}.jpg" in match['value']:
                    waiting = False
                    obj.status = 'Conversion complete'
                    dispatcher.send(signal='Status Update', data=obj)
            if count > 10 and not conversion_started:
                obj.status = 'Failed, Conversion never started'
                dispatcher.send(signal='Status Update', data=obj)
                return False
            time.sleep(1)

        if waiting:
            obj.status = 'Timed out waiting for conversion'
            dispatcher.send(signal='Status Update', data=obj)
            return False
        return True

    def assign_image_to_playlist(self, obj, session, image_name, scaleselect='1920x1080'):
        # based on the scaleselect we build our post data
        # first a dictionary with all possible options, there are more options on the playlist page but for now we will use defaults
        playlistmodes = {'1024x768': '0,136,160,24,1024,6,29,768,3,31,255,25,136,2,9,191,0,0,0,64804',
                         '1920x1080': '0,44,148,638,1920,5,36,1080,4,62,116,10,41,1,9,191,0,0,0,74250',
                         '1280x800': '0,136,200,64,1280,3,24,800,2,31,255,25,61,3,9,191,0,0,1,82998',
                         '1400x1050': '0,128,208,16,1400,3,36,1050,1,31,255,25,123,4,9,191,0,0,1,114750',
                         '1920x1200': '0,32,80,48,1920,6,26,1200,3,31,255,25,208,4,9,187,0,0,1,154000',
                         '720x480': '0,62,60,16,720,6,30,480,9,31,255,25,56,4,9,187,4,0,0,27000',
                         '1280x720': '0,40,220,440,1280,5,20,720,5,62,116,10,41,1,9,191,0,0,0,74250'}
        # Set the playlist
        data = {'executesave': 'doLocalPlay',
                'playlistnum': '1',
                'playlistname': 'Default PlayList 1',
                'playlistmode': playlistmodes[scaleselect],
                "modewidth": scaleselect.split('x')[0],
                "modeheight": scaleselect.split('x')[1],
                "playlistaudio": "-1",
                "image0bin": f"LP_{image_name}_{scaleselect}.{self.svsi_file_type}",
                "image0": image_name,
                "delay0": "1",
                "playlistorder": f"LP_{image_name}_{scaleselect}.{self.svsi_file_type}"}
        try:
            r = session.post(f'{self.protocol}://{obj.ip_address}/localplay.php', data=data, verify=False)
        except Exception as error:
                obj.status = f'Unable to connect to unit: ' + str(error)
                dispatcher.send(signal='Status Update', data=obj)
                return False
        if r.status_code == 200:
            obj.status = f'Assigning to playlist'
            dispatcher.send(signal='Status Update', data=obj)
        else:
            obj.status = f'Unable to assign to playlist. Error {r.status_code}'
            dispatcher.send(signal='Status Update', data=obj)
            return False
        return True

    def get_netlinx(self, job):
        obj = job[1]
        prefs = job[2]

        # login
        obj.status = 'logging in'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            session = self.login(ip_address=obj.ip_address, username=prefs.svsi_username, password=prefs.get_svsi_password())
        except Exception as error:
            print(error)
            obj.status = 'Connection error logging in'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if session is None:
            # print('failed to login')
            obj.status = 'failed to login'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        obj.status = 'getting netlinx'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            req = session.get(f"{self.protocol}://{obj.ip_address}/netlinx.php", verify=False)
        except Exception:
            obj.status = 'Connection Error'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if req.status_code != 200:
            # print('Unable to set netlinx')
            obj.status = 'Unable to get netlinx'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        # search page for auto / listen / url
        # set mode

        try:
            # print(req.text)
            pattern = re.compile("""selected='selected' >(?P<mastermode>.*?)<""")
            obj.master_mode = pattern.search(req.text)['mastermode']
            pattern = re.compile('id="devicenumber\"  value=\"(?P<devicenumber>\\d*?)\"')
            # print(req.text)
            # print(pattern.search(req.text))
            obj.netlinx_device = pattern.search(req.text)['devicenumber']
        except Exception as error:
            print('Unable to parse netlinx: ', error)
            obj.status = 'Connection Error'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return

        data = {'getnetlinx': 'true'}
        try:
            req = session.post(f"{self.protocol}://{obj.ip_address}/netlinx.php", data=data, verify=False)
        except Exception:
            obj.status = 'Connection error while verify netlinx'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if req.status_code != 200:
            # print('')
            obj.status = 'Unable to verify netlinx'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        obj.raw_netlinx = req.text
        obj.get_netlinx_populate()
        # update netlinx
        obj.status = "Success"
        dispatcher.send(signal='Status Update', data=obj)

    def set_netlinx(self, job):
        obj = job[1]
        data = job[2]
        prefs = job[3]

        # login
        obj.status = 'logging in'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            session = self.login(ip_address=obj.ip_address, username=prefs.svsi_username, password=prefs.get_svsi_password())
        except Exception:
            obj.status = 'Connection error logging in'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if session is None:
            # print('failed to login')
            obj.status = 'failed to login'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        obj.status = 'setting netlinx'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            req = session.post(f"{self.protocol}://{obj.ip_address}/netlinx.php", data=data, verify=False)
        except Exception:
            obj.status = 'Connection Error'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if req.status_code != 200:
            # print('Unable to set netlinx')
            obj.status = 'Unable to set netlinx'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        obj.status = 'waiting for connection'
        dispatcher.send(signal='Status Update', data=obj)
        count = 0
        while True:
            count += 1
            data = {'getnetlinx': 'true'}
            try:
                req = session.post(f"{self.protocol}://{obj.ip_address}/netlinx.php", data=data, verify=False)
            except Exception:
                obj.status = 'Connection error while verify netlinx'
                obj.netlinx_status = 'Unknown'
                dispatcher.send(signal='Status Update', data=obj)
                return
            if req.status_code != 200:
                # print('')
                obj.status = 'Unable to verify netlinx'
                obj.netlinx_status = 'Unknown'
                dispatcher.send(signal='Status Update', data=obj)
                return
            obj.raw_netlinx = req.text
            obj.get_netlinx_populate()
            if count >= 15:
                break
            else:
                time.sleep(1)
        obj.status = obj.netlinx_status
        try:
            req = session.get(f"{self.protocol}://{obj.ip_address}/netlinx.php", verify=False)
        except Exception:
            obj.status = 'Connection Error'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
        if req.status_code != 200:
            # print('Unable to set netlinx')
            obj.status = 'Unable to get netlinx'
            obj.netlinx_status = 'Unknown'
            dispatcher.send(signal='Status Update', data=obj)
            return
        # search page for auto / listen / url
        # set mode
        pattern = re.compile("""selected='selected' >(?P<mastermode>.*?)<""")
        obj.master_mode = pattern.search(req.text)['mastermode']
        pattern = re.compile('id="devicenumber\"  value=\"(?P<devicenumber>\\d*?)\"')
        obj.netlinx_device = pattern.search(req.text)['devicenumber']
        dispatcher.send(signal='Status Update', data=obj)

    def set_password(self, job):
        obj = job[1]
        prefs = job[2]
        current_password = job[3]
        new_password = job[4]

        # login
        obj.status = 'logging in'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            session = self.login(ip_address=obj.ip_address, username=prefs.svsi_username, password=current_password)
        except Exception as error:
            logger.debug(f'Unable to connect: {error}')
            obj.status = 'Connection error logging in'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if session is None:
            # print('failed to login')
            obj.status = 'failed to login'
            dispatcher.send(signal='Status Update', data=obj)
            return
        obj.status = 'Changing password'
        dispatcher.send(signal='Status Update', data=obj)
        try:
            data = {'ChangePassword': 'ChangePassword', 'oldpw': current_password, 'newpw': new_password, 'confirmpw': new_password}
            req = session.post(f"{self.protocol}://{obj.ip_address}/login.php", data=data,  verify=False)

        except Exception:
            obj.status = 'Connection Error'
            dispatcher.send(signal='Status Update', data=obj)
            return
        if req.status_code != 200 or req.content != b'1':
            obj.status = 'Unable to set password'
            dispatcher.send(signal='Status Update', data=obj)
            return
        session.get(f"{self.protocol}://{obj.ip_address}/login.php?logout=true")
        obj.status = 'Success'
        dispatcher.send(signal='Status Update', data=obj)



def status(status):
    pprint(status)


def main():
    dispatcher.connect(status,
                       signal="webAPI",
                       sender=dispatcher.Any)
    webapi_queue = Queue()
    webapi_job_thread = WebActionsThread(webapi_queue)
    webapi_job_thread.setDaemon(True)
    webapi_job_thread.start()
    # webapi_queue.put(['webapi_request', 'http://10.1.6.168/', "QueryClients", my_filter])
    # webapi_queue.put(['webapi_request', 'http://10.1.7.255/', "QueryClients", my_filter])
    webapi_job_thread.join()


if __name__ == '__main__':
    main()
