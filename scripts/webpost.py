import requests
import re
import time
import pickle
from pprint import pprint


ip_address = '172.17.162'

s = requests.Session()

req = s.get(f"http://{ip_address}/login.php")


data = {'username': 'admin', 'password': 'password', 'Login': 'Login'}
# cookies = s.cookies.get_dict()
req = s.post(f"http://{ip_address}/login.php", data=data)
print("Logging in", req.status_code)

# my_file = "daniel-polevoy-rE1Ur4OUP7c-unsplash.jpg"
# # Check if file exists:
# checking_for_dups = True
# count = 0
# while checking_for_dups and count < 30:
#     count += 1
#     req = s.get(f"http://{ip_address}/localplay.php")
#     # print(req.text)
#     pattern = re.compile("""\<option title=(?P<name>\".*\")\s*data-width=(?P<width>\".*\")\s*data-height=(?P<height>\".*\")\s*data-filename=(?P<filename>\".*\")\s*value=(?P<value>\".*\")""")
#     for match in pattern.finditer(req.text):
#         # print(match['value'])
#         if f"LP_{my_file}_1920x1080.raw.jpg" in match['value']:
#             # delete duplicate
#             data = {'DeleteLPFile': 'DeleteLPFile', 'file0': f"LP_{my_file}_1920x1080.raw"}
#             r = s.post(f'http://{ip_address}/localplay.php', data=data)
#             print('Found duplicate, deleting it', r.status_code)
#             time.sleep(1)
#             continue
#     print('No duplicates found')
#     checking_for_dups = False

# # add file

# files = {'newimageaudiofileArray[]': (my_file, open(my_file, 'rb'))}
# data = {'keepaspect': 'false', 'fillchoose': '#000000', 'scaleselect': '1920x1080', 'executesave': 'executesave'}
# r = s.post(f'http://{ip_address}/localplay.php', data=data, files=files)
# print('Adding new file', r.status_code)
# # quit()

# # Check conversion status


# # # delete file
# # data = {'DeleteLPFile': 'DeleteLPFile', 'file0': 'LP_test.jpg_1280x720.bin', 'file1': 'LP_white43.jpg_1280x720.bin'}
# # r = s.post(f'http://{ip_address}/localplay.php', data=data)
# # print(r.status_code)

# # assign file to playlist
# # we need to get the avaliable playlist first
# waiting = True
# count = 0
# while waiting and count < 120:
#     count += 1
#     req = s.get(f"http://{ip_address}/localplay.php")
#     # print(req.text)
#     pattern = re.compile("""\<option title=(?P<name>\".*\")\s*data-width=(?P<width>\".*\")\s*data-height=(?P<height>\".*\")\s*data-filename=(?P<filename>\".*\")\s*value=(?P<value>\".*\")""")
#     print(f'Converting, elapsed seconds: {count}')
#     for match in pattern.finditer(req.text):
#         # print(match['value'])
#         if f"LP_{my_file}_1920x1080.raw.jpg" in match['value']:
#             waiting = False
#             print('Conversion complete')
#     time.sleep(1)

# if waiting:
#     print('timed out')
#     quit()


# # # # pattern = re.compile('<option data-width=\"(?P<width>\\d*?)\" data-height=\"(?P<height>\\d*?)\"  data-rawfile=\"(?P<raw>[\\d,\\,]*?)\"[\\s\\S]*?value=\"(?P<value>[\\d,\\,]*?)\">(?P<name>.*?)</option>')
# # # # pattern = re.compile("""(<input id=\"(?P<playlist_id>name_playlist\d)[\s\S]*?value=\"(?P<playlist_name>.*?)\"[\s\S]*?[\s\S]*?data-width=\"(?P<datawidth>\d*)\" data-height=\"(?P<dataheight>\d*)\"  data-rawfile=\"(?P<datarawfile>.*?)\"[\s\S]*?value=\"(?P<currentvalue>[\d,\,]*?)\"[\s\S]*?>(?P<currentname>.*?)<)""")
# req = s.get(f"http://{ip_address}/localplay.php")
# # print(req.text)
# print(re.search("""<option data-padname=\'(?P<image>.*?)\&nbsp;""", req.text)['image'])
# quit()
# pattern = re.compile("""(<input id=\"(?P<playlist_id>name_playlist\d)[\s\S]*?value=\"(?P<playlist_name>.*?)\"[\s\S]*?[\s\S]*?data-width=\"(?P<datawidth>\d*)\" data-height=\"(?P<dataheight>\d*)\"  data-rawfile=\"(?P<datarawfile>.*?)\"[\s\S]*?value=\"(?P<currentvalue>[\d,\,]*?)\"[\s\S]*?>(?P<currentname>.*?)</option>)?<option data-width=\"(?P<width>\d*?)\" data-height=\"(?P<height>\d*?)\"  data-rawfile=\"(?P<raw>[\d,\,]*?)\"[\s\S]*?value=\"(?P<value>[\d,\,]*?)\">(?P<name>.*?)</option>""")
# for m in pattern.finditer(req.text):
#     # print(m.groupdict())
#     my_dict = m.groupdict()
#     if my_dict['playlist_name'] is not None:
#         # print(f"{my_dict['playlist_name']} Output mode: {my_dict['width']} {my_dict['height']} {my_dict['name']}")
#         # print(my_dict)
#         # now find options
#         if my_dict['playlist_name'] == "Default PlayList 1":
#             pass

#     else:
#         print(my_dict)

# # Just blindly set the playlist
# data = {'executesave': 'doLocalPlay',
#         'playlistnum': '1',
#         'playlistname': 'Default PlayList 1',
#         'playlistmode': '0,44,148,88,1920,5,36,1080,4,31,255,25,164,4,9,187,0,0,1,148500',
#         "modewidth": "1920",
#         "modeheight": "1080",
#         "playlistaudio": "-1",
#         "image0bin": f"LP_{my_file}_1920x1080.raw",
#         "image0": my_file,
#         "delay0": "1",
#         "playlistorder": f"LP_{my_file}_1920x1080.raw"}
# r = s.post(f'http://{ip_address}/localplay.php', data=data)
# print("Assigning to playlist", r.status_code)



# opt_pattern = re.compile("""<option data-width=\"(?P<width>\d*?)\" data-height=\"(?P<height>\d*?)\"  data-rawfile=\"(?P<raw>[\d,\,]*?)\"[\s\S]*?value=\"(?P<value>[\d,\,]*?)\">(?P<name>.*?)</option>""")
# for match in opt_pattern.finditer(req.text):
#     # print(m.groupdict())
#     my_dict = m.groupdict()



# N1222 
                                                                  
# data = {'enable': 'on',
#         'mastermode': 'url',
#         'ipurl': '192.168.77.66',
#         'port': '1319',
#         'devicenumber': '8009',
#         'username': '',
#         'password': '',
#         'changenetlinx': 'Save'}
# req = s.post("http://192.168.7.123/netlinx.php", data=data)
# print(req.text)
# time.sleep(2)
# data = {'getnetlinx': 'true'}
# req = s.post(f"http://{ip_address}/netlinx.php", data=data)
# print(req.text)


# my_items = []
# for items in re.findall(pattern, req.text):
#     my_items.append(items)

# with open('test_data.pkl', 'wb') as f:
#     pickle.dump(my_items, f)

# with open('test_data.pkl', 'rb') as f:
#     my_items = pickle.load(f)

# pprint(my_items[0][0])




# req = s.Request("POST", "http://192.168.7.160/login.php")
# prepared = req.prepare()
# print(prepared.headers)

#N2300
# req = s.get("http://192.168.7.137/js/main.js")
# pattern = re.compile('Cookies.set\\(\'(?P<name>.*?)\'')

# cookie_name = pattern.search(req.text)['name']


# data = {'username': 'admin', 'password': 'password'}
# req = s.post("http://192.168.7.137/cgi-bin/acceptpost.cgi", data)

# cookies = {cookie_name: req.text.strip()}
# # cookies = dict(cookie_name=req.text)
# # print(cookies)
# # req = s.post("http://192.168.7.137/cgi-bin/fetchdata.cgi", data=cookies)
# # print(req.text)
# # quit()
# data_form = {'getnetlinx': True}
# cookie_value = req.text.strip()
# headers = {
#            # 'Host': '192.168.7.137',
#            # # 'Connection': 'keep-alive',
#            # # 'Content-Length': '15',
#            # 'Origin': 'http://192.168.7.137',
#            # 'Accept': '*/*',
#            # 'X-Requested-With': 'XMLHttpRequest',
#            # 'DNT': '1',
#            # 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
#            # 'Referer': 'http://192.168.7.137/netlinx.html',
#            # 'Accept-Encoding': 'gzip, deflate',
#            # 'Accept-Language': 'en-US,en;q=0.9,en-AU;q=0.8',
#            'Cookie': f'{cookie_name}={cookie_value}'}

# req = s.post("http://192.168.7.137/cgi-bin/fetchdata.cgi", data='getnetlinx=true')
# print(req.text)

# req = s.post("http://192.168.7.137/cgi-bin/acceptpost.cgi", data='netlinxEnable=false&mastermode=auto&ipurl=192.168.7.67&port=1319&devicenumber=1111&systemnumber=1&username=test&newpassword=mypass')
# print(req.text)
# time.sleep(2)
# req = s.post("http://192.168.7.137/cgi-bin/fetchdata.cgi", data='getnetlinx=true')
# print(req.text)
# req = s.post("http://192.168.7.137/cgi-bin/sendcommand.cgi", data='saveSettings=1&isEncoder=false&settingsLock=true')
# print(req.text)

# files = {'upload_file.jpg': open('test.jpg', 'rb')}
# r = requests.post('http://192.168.7.160/localplay.php', files=files)
# print(r.text)

# r = requests.post('http://192.168.7.160/cgi-bin/localplay.cgi', data='command=saveplaylists&slot1=test.jpg&slot2=test.jpg&slot3=default.jpg&slot4=default.jpg&slot5=default.jpg&slot6=default.jpg&slot7=default.jpg&slot8=default.jpg')