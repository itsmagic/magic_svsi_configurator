# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MagicSvsiConfiguratorFrame
###########################################################################

class MagicSvsiConfiguratorFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 700,500 ), wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button1 = wx.Button( self.m_panel1, wx.ID_ANY, u"Broadcast Search", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button1.SetToolTip( u"Send a discovery broadcast on every interface" )

		bSizer4.Add( self.m_button1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_button9 = wx.Button( self.m_panel1, wx.ID_ANY, u"IP Search", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button9.SetToolTip( u"Search by IP" )

		bSizer4.Add( self.m_button9, 0, wx.ALL, 5 )


		bSizer2.Add( bSizer4, 0, wx.EXPAND, 5 )

		self.olv_panel = wx.Panel( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.olv_sizer = wx.BoxSizer( wx.VERTICAL )


		self.olv_panel.SetSizer( self.olv_sizer )
		self.olv_panel.Layout()
		self.olv_sizer.Fit( self.olv_panel )
		self.m_menu2 = wx.Menu()
		self.rc_menu_status = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Get Status", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.rc_menu_status )

		self.rc_menu_delete = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Delete", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.rc_menu_delete )

		self.rc_menu_host_local = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Add host/localplay image", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.rc_menu_host_local )

		self.rc_menu_netlinx = wx.Menu()
		self.rc_menu_get_netlinx = wx.MenuItem( self.rc_menu_netlinx, wx.ID_ANY, u"Get Netlinx", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu_netlinx.Append( self.rc_menu_get_netlinx )

		self.rc_menu_set_netlinx = wx.MenuItem( self.rc_menu_netlinx, wx.ID_ANY, u"Set Netlinx", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu_netlinx.Append( self.rc_menu_set_netlinx )

		self.m_menu2.AppendSubMenu( self.rc_menu_netlinx, u"Netlinx" )

		self.firmware_rc_menu = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Send Firmware", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.firmware_rc_menu )

		self.rc_menu_hdcp = wx.Menu()
		self.rc_enable_menu = wx.Menu()
		self.rc_menu_auto_hdcp = wx.MenuItem( self.rc_enable_menu, wx.ID_ANY, u"HDCP auto", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_enable_menu.Append( self.rc_menu_auto_hdcp )

		self.rc_menu_on_hdcp = wx.MenuItem( self.rc_enable_menu, wx.ID_ANY, u"HDCP on", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_enable_menu.Append( self.rc_menu_on_hdcp )

		self.rc_menu_hdcp.AppendSubMenu( self.rc_enable_menu, u"Enable HDCP" )

		self.rc_menu_dis_hdcp = wx.MenuItem( self.rc_menu_hdcp, wx.ID_ANY, u"Disable HDCP", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu_hdcp.Append( self.rc_menu_dis_hdcp )

		self.m_menu2.AppendSubMenu( self.rc_menu_hdcp, u"HDCP" )

		self.rc_menu_txcontrol = wx.Menu()
		self.rc_menu_txenable = wx.MenuItem( self.rc_menu_txcontrol, wx.ID_ANY, u"Enable TX", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu_txcontrol.Append( self.rc_menu_txenable )

		self.rc_menu_txdisable = wx.MenuItem( self.rc_menu_txcontrol, wx.ID_ANY, u"Disable TX", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu_txcontrol.Append( self.rc_menu_txdisable )

		self.m_menu2.AppendSubMenu( self.rc_menu_txcontrol, u"TX Control" )

		self.re_menu_aes_67 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"AES 67", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.re_menu_aes_67 )

		self.rc_menu_api = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Send API Command", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.rc_menu_api )

		self.rc_menu_password = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Change Password", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.rc_menu_password )

		self.rc_menu_browser = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Open in browser", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.rc_menu_browser )

		self.olv_panel.Bind( wx.EVT_RIGHT_DOWN, self.olv_panelOnContextMenu )

		bSizer2.Add( self.olv_panel, 1, wx.EXPAND, 5 )


		self.m_panel1.SetSizer( bSizer2 )
		self.m_panel1.Layout()
		bSizer2.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem6 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Preferences", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem6 )

		self.m_menuItem8 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"IP Search", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem8 )

		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem1 )

		self.m_menubar1.Append( self.m_menu1, u"File" )

		self.m_menu4 = wx.Menu()
		self.m_menuItem16 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"General", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.Append( self.m_menuItem16 )

		self.m_menuItem17 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Netlinx", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.Append( self.m_menuItem17 )

		self.m_menuItem18 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"LLDP", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.Append( self.m_menuItem18 )

		self.m_menuItem19 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Firmware", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.Append( self.m_menuItem19 )

		self.m_menubar1.Append( self.m_menu4, u"View" )

		self.m_menu5 = wx.Menu()
		self.m_menuItem23 = wx.MenuItem( self.m_menu5, wx.ID_ANY, u"Manually Add IP", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu5.Append( self.m_menuItem23 )

		self.m_menubar1.Append( self.m_menu5, u"Tools" )

		self.m_menu3 = wx.Menu()
		self.m_menuItem13 = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu3.Append( self.m_menuItem13 )

		self.m_menubar1.Append( self.m_menu3, u"About" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.on_close )
		self.m_button1.Bind( wx.EVT_BUTTON, self.on_broadcast )
		self.m_button9.Bind( wx.EVT_BUTTON, self.on_add_by_ip )
		self.Bind( wx.EVT_MENU, self.on_get_status, id = self.rc_menu_status.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete, id = self.rc_menu_delete.GetId() )
		self.Bind( wx.EVT_MENU, self.on_add_localplay, id = self.rc_menu_host_local.GetId() )
		self.Bind( wx.EVT_MENU, self.on_get_netlinx, id = self.rc_menu_get_netlinx.GetId() )
		self.Bind( wx.EVT_MENU, self.on_set_netlinx, id = self.rc_menu_set_netlinx.GetId() )
		self.Bind( wx.EVT_MENU, self.on_send_firmware, id = self.firmware_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_auto_hdcp, id = self.rc_menu_auto_hdcp.GetId() )
		self.Bind( wx.EVT_MENU, self.on_enable_hdcp, id = self.rc_menu_on_hdcp.GetId() )
		self.Bind( wx.EVT_MENU, self.on_disable_hdcp, id = self.rc_menu_dis_hdcp.GetId() )
		self.Bind( wx.EVT_MENU, self.on_txcontrol, id = self.rc_menu_txenable.GetId() )
		self.Bind( wx.EVT_MENU, self.on_txcontrol, id = self.rc_menu_txdisable.GetId() )
		self.Bind( wx.EVT_MENU, self.on_set_aes67, id = self.re_menu_aes_67.GetId() )
		self.Bind( wx.EVT_MENU, self.on_api_command, id = self.rc_menu_api.GetId() )
		self.Bind( wx.EVT_MENU, self.on_password_change, id = self.rc_menu_password.GetId() )
		self.Bind( wx.EVT_MENU, self.on_browser, id = self.rc_menu_browser.GetId() )
		self.Bind( wx.EVT_MENU, self.on_preferences, id = self.m_menuItem6.GetId() )
		self.Bind( wx.EVT_MENU, self.on_add_by_ip, id = self.m_menuItem8.GetId() )
		self.Bind( wx.EVT_MENU, self.on_exit, id = self.m_menuItem1.GetId() )
		self.Bind( wx.EVT_MENU, self.on_view_general, id = self.m_menuItem16.GetId() )
		self.Bind( wx.EVT_MENU, self.on_view_netlinx, id = self.m_menuItem17.GetId() )
		self.Bind( wx.EVT_MENU, self.on_view_lldp, id = self.m_menuItem18.GetId() )
		self.Bind( wx.EVT_MENU, self.on_view_firmware, id = self.m_menuItem19.GetId() )
		self.Bind( wx.EVT_MENU, self.on_manual_add, id = self.m_menuItem23.GetId() )
		self.Bind( wx.EVT_MENU, self.on_about_box, id = self.m_menuItem13.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_close( self, event ):
		event.Skip()

	def on_broadcast( self, event ):
		event.Skip()

	def on_add_by_ip( self, event ):
		event.Skip()

	def on_get_status( self, event ):
		event.Skip()

	def on_delete( self, event ):
		event.Skip()

	def on_add_localplay( self, event ):
		event.Skip()

	def on_get_netlinx( self, event ):
		event.Skip()

	def on_set_netlinx( self, event ):
		event.Skip()

	def on_send_firmware( self, event ):
		event.Skip()

	def on_auto_hdcp( self, event ):
		event.Skip()

	def on_enable_hdcp( self, event ):
		event.Skip()

	def on_disable_hdcp( self, event ):
		event.Skip()

	def on_txcontrol( self, event ):
		event.Skip()


	def on_set_aes67( self, event ):
		event.Skip()

	def on_api_command( self, event ):
		event.Skip()

	def on_password_change( self, event ):
		event.Skip()

	def on_browser( self, event ):
		event.Skip()

	def on_preferences( self, event ):
		event.Skip()


	def on_exit( self, event ):
		event.Skip()

	def on_view_general( self, event ):
		event.Skip()

	def on_view_netlinx( self, event ):
		event.Skip()

	def on_view_lldp( self, event ):
		event.Skip()

	def on_view_firmware( self, event ):
		event.Skip()

	def on_manual_add( self, event ):
		event.Skip()

	def on_about_box( self, event ):
		event.Skip()

	def olv_panelOnContextMenu( self, event ):
		self.olv_panel.PopupMenu( self.m_menu2, event.GetPoint() )


###########################################################################
## Class CommandIPInterface
###########################################################################

class CommandIPInterface ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Select IP for units to get firmware from", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer33 = wx.BoxSizer( wx.VERTICAL )

		self.date_txt = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer34 = wx.BoxSizer( wx.VERTICAL )

		sbSizer11 = wx.StaticBoxSizer( wx.StaticBox( self.date_txt, wx.ID_ANY, u"Select IP address" ), wx.VERTICAL )

		bSizer38 = wx.BoxSizer( wx.HORIZONTAL )

		self.message_txt = wx.StaticText( sbSizer11.GetStaticBox(), wx.ID_ANY, u"Please select the interface upgrading units have access to", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.message_txt.Wrap( -1 )

		bSizer38.Add( self.message_txt, 1, wx.ALL|wx.EXPAND, 5 )


		sbSizer11.Add( bSizer38, 0, wx.EXPAND, 5 )

		bSizer35 = wx.BoxSizer( wx.VERTICAL )

		interface_cmbChoices = [ u"Choice 1", u"Choice 2" ]
		self.interface_cmb = wx.Choice( sbSizer11.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, interface_cmbChoices, 0 )
		self.interface_cmb.SetSelection( 0 )
		bSizer35.Add( self.interface_cmb, 0, wx.ALL|wx.EXPAND, 5 )

		m_sdbSizer1 = wx.StdDialogButtonSizer()
		self.m_sdbSizer1OK = wx.Button( sbSizer11.GetStaticBox(), wx.ID_OK )
		m_sdbSizer1.AddButton( self.m_sdbSizer1OK )
		self.m_sdbSizer1Cancel = wx.Button( sbSizer11.GetStaticBox(), wx.ID_CANCEL )
		m_sdbSizer1.AddButton( self.m_sdbSizer1Cancel )
		m_sdbSizer1.Realize();

		bSizer35.Add( m_sdbSizer1, 1, wx.ALL|wx.EXPAND, 5 )


		sbSizer11.Add( bSizer35, 0, wx.EXPAND, 5 )


		bSizer34.Add( sbSizer11, 1, wx.EXPAND, 5 )


		self.date_txt.SetSizer( bSizer34 )
		self.date_txt.Layout()
		bSizer34.Fit( self.date_txt )
		bSizer33.Add( self.date_txt, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer33 )
		self.Layout()
		bSizer33.Fit( self )

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class LocalPlay
###########################################################################

class LocalPlay ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add Local/Hostplay image", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.Size( 300,-1 ), wx.DefaultSize )

		bSizer27 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel6 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer28 = wx.BoxSizer( wx.VERTICAL )

		bSizer28.SetMinSize( wx.Size( 250,-1 ) )
		sbSizer8 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel6, wx.ID_ANY, u"Hostplay Image Options" ), wx.VERTICAL )

		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText14 = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"File: ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText14.Wrap( -1 )

		bSizer32.Add( self.m_staticText14, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.file_name_txt = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"File Name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.file_name_txt.Wrap( -1 )

		bSizer32.Add( self.file_name_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer8.Add( bSizer32, 0, wx.EXPAND, 5 )

		sbSizer9 = wx.StaticBoxSizer( wx.StaticBox( sbSizer8.GetStaticBox(), wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		bSizer29 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText12 = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Scale", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )

		bSizer29.Add( self.m_staticText12, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		scale_cmbChoices = []
		self.scale_cmb = wx.ComboBox( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, scale_cmbChoices, 0 )
		bSizer29.Add( self.scale_cmb, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer9.Add( bSizer29, 1, wx.EXPAND, 5 )

		bSizer30 = wx.BoxSizer( wx.VERTICAL )

		self.keep_aspect_chk = wx.CheckBox( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Keep Aspect", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer30.Add( self.keep_aspect_chk, 0, wx.ALL, 5 )


		sbSizer9.Add( bSizer30, 1, wx.EXPAND|wx.ALL, 5 )


		sbSizer8.Add( sbSizer9, 1, wx.EXPAND|wx.ALL, 5 )

		bSizer33 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button11 = wx.Button( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer33.Add( self.m_button11, 0, wx.ALL, 5 )

		self.m_button10 = wx.Button( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Submit", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer33.Add( self.m_button10, 0, wx.ALL, 5 )


		sbSizer8.Add( bSizer33, 0, wx.ALIGN_RIGHT|wx.RIGHT, 5 )


		bSizer28.Add( sbSizer8, 0, wx.EXPAND|wx.ALL, 5 )


		self.m_panel6.SetSizer( bSizer28 )
		self.m_panel6.Layout()
		bSizer28.Fit( self.m_panel6 )
		bSizer27.Add( self.m_panel6, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer27 )
		self.Layout()
		bSizer27.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button11.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_button10.Bind( wx.EVT_BUTTON, self.on_submit )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_cancel( self, event ):
		event.Skip()

	def on_submit( self, event ):
		event.Skip()


###########################################################################
## Class Firmware
###########################################################################

class Firmware ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Firmware Selection", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.Size( 300,-1 ), wx.DefaultSize )

		bSizer33 = wx.BoxSizer( wx.VERTICAL )

		self.date_txt = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer34 = wx.BoxSizer( wx.VERTICAL )

		sbSizer11 = wx.StaticBoxSizer( wx.StaticBox( self.date_txt, wx.ID_ANY, u"Firmware" ), wx.VERTICAL )

		bSizer54 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText16 = wx.StaticText( sbSizer11.GetStaticBox(), wx.ID_ANY, u"Model", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText16.Wrap( -1 )

		self.m_staticText16.SetMinSize( wx.Size( 80,-1 ) )

		bSizer54.Add( self.m_staticText16, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.model_txt = wx.TextCtrl( sbSizer11.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.model_txt.Enable( False )

		bSizer54.Add( self.model_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer11.Add( bSizer54, 1, wx.EXPAND, 5 )

		bSizer35 = wx.BoxSizer( wx.VERTICAL )

		firmware_files_cmbChoices = [ u"Choice 1", u"Choice 2" ]
		self.firmware_files_cmb = wx.Choice( sbSizer11.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, firmware_files_cmbChoices, 0 )
		self.firmware_files_cmb.SetSelection( 0 )
		bSizer35.Add( self.firmware_files_cmb, 0, wx.ALL|wx.EXPAND, 5 )

		m_sdbSizer1 = wx.StdDialogButtonSizer()
		self.m_sdbSizer1OK = wx.Button( sbSizer11.GetStaticBox(), wx.ID_OK )
		m_sdbSizer1.AddButton( self.m_sdbSizer1OK )
		self.m_sdbSizer1Cancel = wx.Button( sbSizer11.GetStaticBox(), wx.ID_CANCEL )
		m_sdbSizer1.AddButton( self.m_sdbSizer1Cancel )
		m_sdbSizer1.Realize();

		bSizer35.Add( m_sdbSizer1, 1, wx.ALL|wx.EXPAND, 5 )


		sbSizer11.Add( bSizer35, 0, wx.EXPAND|wx.ALL, 5 )


		bSizer34.Add( sbSizer11, 1, wx.EXPAND|wx.ALL, 5 )


		self.date_txt.SetSizer( bSizer34 )
		self.date_txt.Layout()
		bSizer34.Fit( self.date_txt )
		bSizer33.Add( self.date_txt, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer33 )
		self.Layout()
		bSizer33.Fit( self )

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class AddByIP
###########################################################################

class AddByIP ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add by IP range", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer28 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel5 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer29 = wx.BoxSizer( wx.VERTICAL )

		bSizer35 = wx.BoxSizer( wx.HORIZONTAL )

		self.range_btn = wx.RadioButton( self.m_panel5, wx.ID_ANY, u"Range", wx.DefaultPosition, wx.DefaultSize, wx.RB_GROUP )
		self.range_btn.SetValue( True )
		bSizer35.Add( self.range_btn, 0, wx.ALL, 5 )

		self.m_radioBtn4 = wx.RadioButton( self.m_panel5, wx.ID_ANY, u"Single", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer35.Add( self.m_radioBtn4, 0, wx.ALL, 5 )


		bSizer29.Add( bSizer35, 0, wx.EXPAND, 5 )

		bSizer30 = wx.BoxSizer( wx.HORIZONTAL )

		self.start_ip_name_txt = wx.StaticText( self.m_panel5, wx.ID_ANY, u"Start IP Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.start_ip_name_txt.Wrap( -1 )

		self.start_ip_name_txt.SetMinSize( wx.Size( 100,-1 ) )

		bSizer30.Add( self.start_ip_name_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.start_ip_txt = wx.TextCtrl( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer30.Add( self.start_ip_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer29.Add( bSizer30, 1, wx.EXPAND, 5 )

		bSizer301 = wx.BoxSizer( wx.HORIZONTAL )

		self.end_ip_name_txt = wx.StaticText( self.m_panel5, wx.ID_ANY, u"End IP Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.end_ip_name_txt.Wrap( -1 )

		self.end_ip_name_txt.SetMinSize( wx.Size( 100,-1 ) )

		bSizer301.Add( self.end_ip_name_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.end_ip_txt = wx.TextCtrl( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer301.Add( self.end_ip_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer29.Add( bSizer301, 1, wx.EXPAND, 5 )

		bSizer41 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText17 = wx.StaticText( self.m_panel5, wx.ID_ANY, u"Interface", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText17.Wrap( -1 )

		self.m_staticText17.SetMinSize( wx.Size( 100,-1 ) )

		bSizer41.Add( self.m_staticText17, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		interface_cmbChoices = []
		self.interface_cmb = wx.Choice( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, interface_cmbChoices, 0 )
		self.interface_cmb.SetSelection( 0 )
		bSizer41.Add( self.interface_cmb, 0, wx.ALL, 5 )


		bSizer29.Add( bSizer41, 1, wx.EXPAND, 5 )

		bSizer34 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button6 = wx.Button( self.m_panel5, wx.ID_ANY, u"Send", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer34.Add( self.m_button6, 0, wx.ALL, 5 )

		self.m_button7 = wx.Button( self.m_panel5, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer34.Add( self.m_button7, 0, wx.ALL, 5 )


		bSizer29.Add( bSizer34, 1, wx.ALIGN_RIGHT, 5 )


		self.m_panel5.SetSizer( bSizer29 )
		self.m_panel5.Layout()
		bSizer29.Fit( self.m_panel5 )
		bSizer28.Add( self.m_panel5, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer28 )
		self.Layout()
		bSizer28.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.range_btn.Bind( wx.EVT_RADIOBUTTON, self.on_range )
		self.m_radioBtn4.Bind( wx.EVT_RADIOBUTTON, self.on_range )
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_send )
		self.m_button7.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_range( self, event ):
		event.Skip()


	def on_send( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()


###########################################################################
## Class ManualIP
###########################################################################

class ManualIP ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Manual Add By IP", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer49 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel10 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer50 = wx.BoxSizer( wx.VERTICAL )

		bSizer51 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText21 = wx.StaticText( self.m_panel10, wx.ID_ANY, u"IP Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText21.Wrap( -1 )

		bSizer51.Add( self.m_staticText21, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.ip_address_txt = wx.TextCtrl( self.m_panel10, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer51.Add( self.ip_address_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer50.Add( bSizer51, 1, wx.EXPAND, 5 )

		bSizer52 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button15 = wx.Button( self.m_panel10, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer52.Add( self.m_button15, 0, wx.ALL, 5 )

		self.m_button16 = wx.Button( self.m_panel10, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer52.Add( self.m_button16, 0, wx.ALL, 5 )


		bSizer50.Add( bSizer52, 1, wx.ALIGN_RIGHT, 5 )


		self.m_panel10.SetSizer( bSizer50 )
		self.m_panel10.Layout()
		bSizer50.Fit( self.m_panel10 )
		bSizer49.Add( self.m_panel10, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer49 )
		self.Layout()
		bSizer49.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button15.Bind( wx.EVT_BUTTON, self.on_add )
		self.m_button16.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_add( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()


###########################################################################
## Class PreferencesMenu
###########################################################################

class PreferencesMenu ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Preferences", pos = wx.DefaultPosition, size = wx.Size( 655,754 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer24 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel4 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer25 = wx.BoxSizer( wx.VERTICAL )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel4, wx.ID_ANY, u"Preferences" ), wx.VERTICAL )

		sbSizer61 = wx.StaticBoxSizer( wx.StaticBox( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		self.multicast_chk = wx.CheckBox( sbSizer61.GetStaticBox(), wx.ID_ANY, u"Multicast Discovery Enable", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.multicast_chk.SetValue(True)
		sbSizer61.Add( self.multicast_chk, 0, wx.ALL, 5 )


		sbSizer2.Add( sbSizer61, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer7 = wx.StaticBoxSizer( wx.StaticBox( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		bSizer28 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText12 = wx.StaticText( sbSizer7.GetStaticBox(), wx.ID_ANY, u"Svsi Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )

		self.m_staticText12.SetMinSize( wx.Size( 100,-1 ) )

		bSizer28.Add( self.m_staticText12, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.svsi_username_txt = wx.TextCtrl( sbSizer7.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer28.Add( self.svsi_username_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer7.Add( bSizer28, 1, wx.EXPAND, 5 )

		bSizer281 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText121 = wx.StaticText( sbSizer7.GetStaticBox(), wx.ID_ANY, u"Svsi Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText121.Wrap( -1 )

		self.m_staticText121.SetMinSize( wx.Size( 100,-1 ) )

		bSizer281.Add( self.m_staticText121, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.svsi_password_txt = wx.TextCtrl( sbSizer7.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD|wx.TE_PROCESS_ENTER )
		bSizer281.Add( self.svsi_password_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer7.Add( bSizer281, 1, wx.EXPAND, 5 )


		sbSizer2.Add( sbSizer7, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer13 = wx.StaticBoxSizer( wx.StaticBox( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		bSizer53 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText22 = wx.StaticText( sbSizer13.GetStaticBox(), wx.ID_ANY, u"Firmware Directory", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText22.Wrap( -1 )

		self.m_staticText22.SetMinSize( wx.Size( 100,-1 ) )

		bSizer53.Add( self.m_staticText22, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.dir_picker_txt = wx.DirPickerCtrl( sbSizer13.GetStaticBox(), wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE|wx.DIRP_DIR_MUST_EXIST )
		bSizer53.Add( self.dir_picker_txt, 1, wx.ALL, 5 )


		sbSizer13.Add( bSizer53, 1, wx.EXPAND, 5 )


		sbSizer2.Add( sbSizer13, 1, wx.EXPAND|wx.ALL, 5 )


		bSizer25.Add( sbSizer2, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer21 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel4, wx.ID_ANY, u"Columns Displayed" ), wx.HORIZONTAL )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( sbSizer21.GetStaticBox(), wx.ID_ANY, u"General" ), wx.VERTICAL )

		self.time_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Time", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.time_chk.SetValue(True)
		self.time_chk.Enable( False )
		self.time_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.time_chk, 0, wx.ALL, 5 )

		self.name_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.name_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.name_chk, 0, wx.ALL, 5 )

		self.type_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Type", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.type_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.type_chk, 0, wx.ALL, 5 )

		self.model_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Model", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer3.Add( self.model_chk, 0, wx.ALL, 5 )

		self.mac_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"MAC", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.mac_chk.SetValue(True)
		self.mac_chk.Enable( False )
		self.mac_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.mac_chk, 0, wx.ALL, 5 )

		self.ip_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"IP", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ip_chk.SetValue(True)
		self.ip_chk.Enable( False )
		self.ip_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.ip_chk, 0, wx.ALL, 5 )

		self.serial_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Serial", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.serial_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.serial_chk, 0, wx.ALL, 5 )

		self.stream_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Stream", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.stream_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.stream_chk, 0, wx.ALL, 5 )

		self.mode_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Mode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.mode_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.mode_chk, 0, wx.ALL, 5 )

		self.resolution_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Resolution", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.resolution_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.resolution_chk, 0, wx.ALL, 5 )

		self.audio_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Audio", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.audio_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.audio_chk, 0, wx.ALL, 5 )

		self.hdcp_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"HDCP", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer3.Add( self.hdcp_chk, 0, wx.ALL, 5 )

		self.aes67_enable_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"AES67 Enable", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer3.Add( self.aes67_enable_chk, 0, wx.ALL, 5 )

		self.aes67_address_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"AES67 Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer3.Add( self.aes67_address_chk, 0, wx.ALL, 5 )

		self.status_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.status_chk.SetValue(True)
		self.status_chk.Enable( False )
		self.status_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer3.Add( self.status_chk, 0, wx.ALL, 5 )


		sbSizer21.Add( sbSizer3, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer6 = wx.StaticBoxSizer( wx.StaticBox( sbSizer21.GetStaticBox(), wx.ID_ANY, u"Netlinx" ), wx.VERTICAL )

		self.netlinx_enable_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Netlinx Enable", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer6.Add( self.netlinx_enable_chk, 0, wx.ALL, 5 )

		self.netlinx_status_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Netlinx Status", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer6.Add( self.netlinx_status_chk, 0, wx.ALL, 5 )

		self.master_mode_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Master Mode", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer6.Add( self.master_mode_chk, 0, wx.ALL, 5 )

		self.master_ip_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Master IP", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer6.Add( self.master_ip_chk, 0, wx.ALL, 5 )

		self.netlinx_device_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Netlinx Device", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer6.Add( self.netlinx_device_chk, 0, wx.ALL, 5 )

		self.netlinx_system_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Netlinx System", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer6.Add( self.netlinx_system_chk, 0, wx.ALL, 5 )


		sbSizer21.Add( sbSizer6, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( sbSizer21.GetStaticBox(), wx.ID_ANY, u"LLDP" ), wx.VERTICAL )

		self.lldp_chassis_id_chk = wx.CheckBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"LLDP Chassis ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lldp_chassis_id_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer4.Add( self.lldp_chassis_id_chk, 0, wx.ALL, 5 )

		self.lldp_sys_name_chk = wx.CheckBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"LLDP SYS Name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lldp_sys_name_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer4.Add( self.lldp_sys_name_chk, 0, wx.ALL, 5 )

		self.lldp_port_desc_chk = wx.CheckBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"LLDP Port Desc", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lldp_port_desc_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer4.Add( self.lldp_port_desc_chk, 0, wx.ALL, 5 )

		self.lldp_port_id_chk = wx.CheckBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"LLDP Port ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lldp_port_id_chk.SetMinSize( wx.Size( 110,-1 ) )

		sbSizer4.Add( self.lldp_port_id_chk, 0, wx.ALL, 5 )


		sbSizer21.Add( sbSizer4, 1, wx.EXPAND|wx.ALL, 5 )

		sbSizer10 = wx.StaticBoxSizer( wx.StaticBox( sbSizer21.GetStaticBox(), wx.ID_ANY, u"Firmware" ), wx.VERTICAL )

		self.firmware_release_chk = wx.CheckBox( sbSizer10.GetStaticBox(), wx.ID_ANY, u"Firmware Release", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer10.Add( self.firmware_release_chk, 0, wx.ALL, 5 )

		self.software_version_chk = wx.CheckBox( sbSizer10.GetStaticBox(), wx.ID_ANY, u"Software Verison", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer10.Add( self.software_version_chk, 0, wx.ALL, 5 )

		self.web_version_chk = wx.CheckBox( sbSizer10.GetStaticBox(), wx.ID_ANY, u"Web Version", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer10.Add( self.web_version_chk, 0, wx.ALL, 5 )


		sbSizer21.Add( sbSizer10, 1, wx.EXPAND|wx.ALL, 5 )


		bSizer25.Add( sbSizer21, 1, wx.EXPAND|wx.ALL, 5 )

		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button4 = wx.Button( self.m_panel4, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.m_button4, 0, wx.ALL, 5 )

		self.m_button5 = wx.Button( self.m_panel4, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.m_button5, 0, wx.ALL, 5 )


		bSizer25.Add( bSizer27, 0, wx.ALIGN_RIGHT, 5 )


		self.m_panel4.SetSizer( bSizer25 )
		self.m_panel4.Layout()
		bSizer25.Fit( self.m_panel4 )
		bSizer24.Add( self.m_panel4, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer24 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.svsi_password_txt.Bind( wx.EVT_TEXT_ENTER, self.on_ok )
		self.dir_picker_txt.Bind( wx.EVT_DIRPICKER_CHANGED, self.on_firmware_directory )
		self.m_button4.Bind( wx.EVT_BUTTON, self.on_save )
		self.m_button5.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_ok( self, event ):
		event.Skip()

	def on_firmware_directory( self, event ):
		event.Skip()

	def on_save( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()


###########################################################################
## Class NetlinxSetup
###########################################################################

class NetlinxSetup ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Netlinx", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.Size( -1,-1 ), wx.DefaultSize )

		bSizer7 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer8 = wx.BoxSizer( wx.VERTICAL )

		bSizer9 = wx.BoxSizer( wx.VERTICAL )

		self.enable_chk = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Enable Netlinx", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.enable_chk, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer9, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Master Mode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		self.m_staticText3.SetMinSize( wx.Size( 100,-1 ) )

		bSizer10.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		mode_cmbChoices = [ u"Auto", u"Listen", u"URL" ]
		self.mode_cmb = wx.ComboBox( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, mode_cmbChoices, 0 )
		self.mode_cmb.SetSelection( 2 )
		bSizer10.Add( self.mode_cmb, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer10, 0, wx.EXPAND|wx.ALL, 5 )

		bSizer101 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText31 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"IP/URL", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText31.Wrap( -1 )

		self.m_staticText31.SetMinSize( wx.Size( 100,-1 ) )

		bSizer101.Add( self.m_staticText31, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.netlinx_ip_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer101.Add( self.netlinx_ip_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer101, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer102 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText32 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText32.Wrap( -1 )

		self.m_staticText32.SetMinSize( wx.Size( 100,-1 ) )

		bSizer102.Add( self.m_staticText32, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.netlinx_port_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer102.Add( self.netlinx_port_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer102, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer103 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText33 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Device Number", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText33.Wrap( -1 )

		self.m_staticText33.SetMinSize( wx.Size( 100,-1 ) )

		bSizer103.Add( self.m_staticText33, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.device_number_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer103.Add( self.device_number_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer103, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer104 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText34 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"System Number", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText34.Wrap( -1 )

		self.m_staticText34.SetMinSize( wx.Size( 100,-1 ) )

		bSizer104.Add( self.m_staticText34, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.system_number_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer104.Add( self.system_number_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer104, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer105 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText35 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText35.Wrap( -1 )

		self.m_staticText35.SetMinSize( wx.Size( 100,-1 ) )

		bSizer105.Add( self.m_staticText35, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.username_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer105.Add( self.username_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer105, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer106 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText36 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText36.Wrap( -1 )

		self.m_staticText36.SetMinSize( wx.Size( 100,-1 ) )

		bSizer106.Add( self.m_staticText36, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.password_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		bSizer106.Add( self.password_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer106, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer41 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button6 = wx.Button( self.m_panel3, wx.ID_ANY, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_button6, 0, wx.ALL, 5 )

		self.m_button7 = wx.Button( self.m_panel3, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_button7, 0, wx.ALL, 5 )

		self.m_button8 = wx.Button( self.m_panel3, wx.ID_ANY, u"Abort", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_button8, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer41, 1, wx.EXPAND|wx.ALL, 5 )


		self.m_panel3.SetSizer( bSizer8 )
		self.m_panel3.Layout()
		bSizer8.Fit( self.m_panel3 )
		bSizer7.Add( self.m_panel3, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer7 )
		self.Layout()
		bSizer7.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_ok )
		self.m_button7.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_button8.Bind( wx.EVT_BUTTON, self.on_abort )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_ok( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()

	def on_abort( self, event ):
		event.Skip()


###########################################################################
## Class AES67Setup
###########################################################################

class AES67Setup ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"AES67", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.Size( -1,-1 ), wx.DefaultSize )

		bSizer7 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer8 = wx.BoxSizer( wx.VERTICAL )

		bSizer9 = wx.BoxSizer( wx.VERTICAL )

		self.enable_chk = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"AES67 Audio", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.enable_chk, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer9, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer101 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText31 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"AES67 Audio Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText31.Wrap( -1 )

		self.m_staticText31.SetMinSize( wx.Size( 140,-1 ) )

		bSizer101.Add( self.m_staticText31, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.aes67_address_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer101.Add( self.aes67_address_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer101, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer102 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText32 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"AES67 Audio Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText32.Wrap( -1 )

		self.m_staticText32.SetMinSize( wx.Size( 140,-1 ) )

		bSizer102.Add( self.m_staticText32, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.aes67_port_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer102.Add( self.aes67_port_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer102, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

		self.channels_static_txt = wx.StaticText( self.m_panel3, wx.ID_ANY, u"AES67 Audio Channels", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.channels_static_txt.Wrap( -1 )

		self.channels_static_txt.SetMinSize( wx.Size( 140,-1 ) )

		bSizer10.Add( self.channels_static_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		channels_cmbChoices = [ u"2", u"8" ]
		self.channels_cmb = wx.ComboBox( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, channels_cmbChoices, 0 )
		self.channels_cmb.SetSelection( 0 )
		bSizer10.Add( self.channels_cmb, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer10, 0, wx.EXPAND|wx.ALL, 5 )

		bSizer41 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button6 = wx.Button( self.m_panel3, wx.ID_ANY, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_button6, 0, wx.ALL, 5 )

		self.m_button7 = wx.Button( self.m_panel3, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_button7, 0, wx.ALL, 5 )

		self.m_button8 = wx.Button( self.m_panel3, wx.ID_ANY, u"Abort", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer41.Add( self.m_button8, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer41, 1, wx.EXPAND|wx.ALL, 5 )


		self.m_panel3.SetSizer( bSizer8 )
		self.m_panel3.Layout()
		bSizer8.Fit( self.m_panel3 )
		bSizer7.Add( self.m_panel3, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer7 )
		self.Layout()
		bSizer7.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_ok )
		self.m_button7.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_button8.Bind( wx.EVT_BUTTON, self.on_abort )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_ok( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()

	def on_abort( self, event ):
		event.Skip()


###########################################################################
## Class PasswordChange
###########################################################################

class PasswordChange ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Change WebUI Password", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE|wx.STAY_ON_TOP )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer54 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel11 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer55 = wx.BoxSizer( wx.VERTICAL )

		bSizer56 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText23 = wx.StaticText( self.m_panel11, wx.ID_ANY, u"Current Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText23.Wrap( -1 )

		self.m_staticText23.SetMinSize( wx.Size( 140,-1 ) )

		bSizer56.Add( self.m_staticText23, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.current_password_txt = wx.TextCtrl( self.m_panel11, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		self.current_password_txt.SetMinSize( wx.Size( 140,-1 ) )

		bSizer56.Add( self.current_password_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.current_password_plain_txt = wx.TextCtrl( self.m_panel11, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.current_password_plain_txt.Hide()
		self.current_password_plain_txt.SetMinSize( wx.Size( 140,-1 ) )

		bSizer56.Add( self.current_password_plain_txt, 0, wx.ALL, 5 )

		self.show_passwords_btn = wx.BitmapButton( self.m_panel11, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.show_passwords_btn.SetBitmapCurrent( wx.NullBitmap )
		bSizer56.Add( self.show_passwords_btn, 0, wx.ALL, 5 )


		bSizer55.Add( bSizer56, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer561 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText231 = wx.StaticText( self.m_panel11, wx.ID_ANY, u"New Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText231.Wrap( -1 )

		self.m_staticText231.SetMinSize( wx.Size( 140,-1 ) )

		bSizer561.Add( self.m_staticText231, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.new_password_txt = wx.TextCtrl( self.m_panel11, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		self.new_password_txt.SetMinSize( wx.Size( 140,-1 ) )

		bSizer561.Add( self.new_password_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.new_password_plain_txt = wx.TextCtrl( self.m_panel11, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.new_password_plain_txt.Hide()
		self.new_password_plain_txt.SetMinSize( wx.Size( 140,-1 ) )

		bSizer561.Add( self.new_password_plain_txt, 0, wx.ALL, 5 )


		bSizer55.Add( bSizer561, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer562 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText232 = wx.StaticText( self.m_panel11, wx.ID_ANY, u"Confirm New Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText232.Wrap( -1 )

		self.m_staticText232.SetMinSize( wx.Size( 140,-1 ) )

		bSizer562.Add( self.m_staticText232, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.confirm_password_txt = wx.TextCtrl( self.m_panel11, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD|wx.TE_PROCESS_ENTER )
		self.confirm_password_txt.SetMinSize( wx.Size( 140,-1 ) )

		bSizer562.Add( self.confirm_password_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer55.Add( bSizer562, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

		bSizer62 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button17 = wx.Button( self.m_panel11, wx.ID_ANY, u"Ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer62.Add( self.m_button17, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_button18 = wx.Button( self.m_panel11, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer62.Add( self.m_button18, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer55.Add( bSizer62, 1, wx.ALIGN_RIGHT|wx.RIGHT|wx.LEFT, 5 )


		self.m_panel11.SetSizer( bSizer55 )
		self.m_panel11.Layout()
		bSizer55.Fit( self.m_panel11 )
		bSizer54.Add( self.m_panel11, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer54 )
		self.Layout()
		bSizer54.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.current_password_plain_txt.Bind( wx.EVT_TEXT, self.on_current_password_plain )
		self.show_passwords_btn.Bind( wx.EVT_BUTTON, self.on_show_passwords )
		self.new_password_txt.Bind( wx.EVT_TEXT, self.on_check_password )
		self.new_password_plain_txt.Bind( wx.EVT_TEXT, self.on_new_password_plain )
		self.confirm_password_txt.Bind( wx.EVT_TEXT, self.on_check_password )
		self.confirm_password_txt.Bind( wx.EVT_TEXT_ENTER, self.on_ok )
		self.m_button17.Bind( wx.EVT_BUTTON, self.on_ok )
		self.m_button18.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_current_password_plain( self, event ):
		event.Skip()

	def on_show_passwords( self, event ):
		event.Skip()

	def on_check_password( self, event ):
		event.Skip()

	def on_new_password_plain( self, event ):
		event.Skip()


	def on_ok( self, event ):
		event.Skip()


	def on_cancel( self, event ):
		event.Skip()


