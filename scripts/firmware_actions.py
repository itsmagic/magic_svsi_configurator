import socket
import time
import logging
from threading import Thread
from pydispatch import dispatcher
from pprint import pprint
from scripts import webserver_class

logger = logging.getLogger("FirmwareActionsThread")

class FirmwareActionsThread(Thread):

    def __init__(self, queue, parent, port=50001):
        Thread.__init__(self)
        self.parent = parent
        self.queue = queue
        self.port = port
        self.file_transfer_complete = False
        self.file_transfer_progress = ''
        self.shutdown = False
        dispatcher.connect(self.unit_upgrading, signal='Unit Upgrading')
        dispatcher.connect(self.unit_progress, signal='HTTP Progress')
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def unit_upgrading(self, sender, data):
        self.file_transfer_complete = True

    def unit_progress(self, sender, data):
        self.file_transfer_progress = data['message']

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def command_unit_to_get_firmware(self, job):
        unit = job[1]
        listen_ip = job[3]
        # command unit to update
        unit.status = 'Commanding unit to download update'
        dispatcher.send(signal='Status Update', data=unit)
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            command = f'setSettings:updateUnit:{listen_ip}'
            s.send(command.encode())
            s.close()
        except Exception as error:
            unit.status = f'Failed to connect to unit: {error}'
            dispatcher.send(signal='Status Update', data=unit)
            return False
        return True

    def send_firmware_to_unit(self, job):
        unit = job[1]
        firmware_path = job[2]
        try:
            my_httpd = webserver_class.SVSIHTTPThread(unit, firmware_path)
            my_httpd.setDaemon(True)
            my_httpd.start()
        except Exception as error:
            unit.status = f'Error starting HTTP Server {error}'
            dispatcher.send(signal='Status Update', data=unit)
            return

        # HTTP is running, we need to command the unit to download the file
        if not self.command_unit_to_get_firmware(job):
            dispatcher.send(signal='Remove Upgrading Unit', data=unit)
            return
        # Wait for download
        self.file_transfer_complete = False
        count = 0
        progress = ''
        while count < 10:
            count += 1
            if self.file_transfer_complete:
                break
            if self.file_transfer_progress != progress:
                # We want to keep the http open for as long as it takes, as long as the unit is downloading we will extend the time
                progress = self.file_transfer_progress
                count = 0

            time.sleep(1)
        # either we have sent the file or it has failed, shutdown httpd
        my_httpd.on_shutdown(None)
        if not self.file_transfer_complete:
            # print('the unit didnt download the file from us')
            # print('Need to update status of unit, and remove from unit verification')
            unit.status = 'Unable to download firmware'
            dispatcher.send(signal='Status Update', data=unit)
            dispatcher.send(signal='Remove Upgrading Unit', data=unit)


def status(status):
    pprint(status)


def main():
    pass


if __name__ == '__main__':
    main()
