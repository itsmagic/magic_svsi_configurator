import datetime
import ipaddress
import logging
from dataclasses import dataclass, field
from cryptography.fernet import Fernet

import os

logger = logging.getLogger("Datastore")

ENCRYPTION_KEY = b'kjSMVBkuTWZXTJ0mLCZFHddMhFGfmlsAw32tms73sDA='
cipher_suite = Fernet(ENCRYPTION_KEY)


@dataclass
class SVSIUnit:
    arrival_time: datetime = datetime.datetime.now()
    name: str = ''
    box_type: str = ''
    model: str = ''
    svsi_type: str = ''
    mac_address: str = ''
    ip_address: str = ''
    netmask_address: str = ''
    gateway_addresses: str = ''
    serial_number: str = ''
    stream: str = ''
    mode: str = ''
    resolution: str = ''
    audio: str = ''
    hdcp: str = ''
    https: str = ''

    # Netlinx items
    netlinx_enable: str = ''
    netlinx_status: str = ''
    master_mode: str = ''
    netlinx_ip: str = ''
    netlinx_port: str = ''
    netlinx_device: str = ''
    netlinx_system: str = ''
    raw_netlinx: str = ''

    # Firmware items
    firmware_release: str = ''
    software_version: str = ''
    web_version: str = ''
    update_val: str = ''
    updtry: str = ''
    updfailed: str = ''
    update_stage: str = ''

    # LLDP
    lldp_sys_name: str = ''
    lldp_port_id: str = ''
    lldp_port_desc: str = ''
    lldp_chassis_id: str = ''
    get_status: dict = field(default_factory=dict)
    raw_multicast: str = ''
    lldp: dict = field(default_factory=dict)

    # AES67
    aes67: str = ''
    aes67address: str = ''

    status: str = ''

    # Resolutions supported
    # [{'dataheight': '1080', 'datawidth': '1920', 'rawfile': '0,44,148,638,1920,5,36,1080,4,62,116,10,41,1,9,191,0,0,0,74250', 'value': '1920x1080'}, ...]
    playlistmodes: list = field(default_factory=list)
    selected_scale: str = '1280x720'

    # File types supported
    # ['jpeg', 'jpg']
    file_types: list = field(default_factory=list)

    def get_status_populate(self):
        # print(self.get_status)
        if 'SVSI_TXGEN2' in self.get_status:
            self.box_type = 'TX'
            if self.serial_number[1] in ['1', '4']:
                self.model = f'{self.serial_number[:2]}000 Encoder'
            else:
                self.model = f'{self.serial_number[:3]}00 Encoder'
        if 'SVSI_RXGEN2' in self.get_status:
            self.box_type = 'RX'
            self.audio = self.get_status['STREAMAUDIO']
            if self.serial_number[1] in ['1', '4']:
                self.model = f'{self.serial_number[:2]}000 Decoder'
            else:
                self.model = f'{self.serial_number[:3]}00 Decoder'

        for item in ['NAME', 'STREAM', 'FCPC', 'CPC', 'AES67', 'AESTX', 'AESRX']:
            if item in self.get_status:
                if item == 'FCPC':
                    if self.get_status['FCPC'] in ["on", "V1"]:
                        self.hdcp = "on"
                    else:
                        self.hdcp = self.get_status['FCPC']
                elif item in ['AESTX', 'AESRX']:
                    self.aes67address = self.get_status[item]
                else:
                    setattr(self, item.lower(), self.get_status[item])
        # if 'NAME' in self.get_status:
        #     self.name = self.get_status['NAME']
        # if 'STREAM' in self.get_status:
        #     self.stream = self.get_status['STREAM']
        if 'PLAYMODE' in self.get_status:
            self.mode = self.get_status['PLAYMODE']
        if 'INPUTRES' in self.get_status:
            self.resolution = self.get_status['INPUTRES']

        # if 'FCPC' in self.get_status:
        #     if self.get_status['FCPC'] in ["on", "V1"]:
        #         self.hdcp = "on"
        #     else:
        #         self.hdcp = self.get_status['FCPC']
        # if 'CPC' in self.get_status:
        #     self.hdcp = self.get_status['CPC']

        firmware_items = {'rel': 'firmware_release',
                          'SWVER': 'software_version',
                          'WEBVER': 'web_version',
                          'UPDATE': 'update_val',
                          'UPDTRY': 'updtry',
                          'UPDFAILED': 'updfailed'}

        for key in firmware_items.keys():
            if key in self.get_status:
                setattr(self, firmware_items[key].lower(), self.get_status[key])

        if 'NM' in self.get_status:
            self.netmask_address = self.get_status['NM']
        if 'GW' in self.get_status:
            self.gateway_addresses = self.get_status['GW']
        if 'HTTPS' in self.get_status:
            self.https = self.get_status['HTTPS']

    def get_lldp_populate(self):
        lldp_items = {'chassisid': 'lldp_chassis_id',
                      'portid': 'lldp_port_id',
                      'portdescr': 'lldp_port_desc',
                      'sysname': 'lldp_sys_name'}

        for key in lldp_items.keys():
            if key in self.lldp:
                setattr(self, lldp_items[key].lower(), self.lldp[key])
        # for item in ['chassisid', 'portid', 'portdescr', 'sysname']:
        #     if item in self.lldp:
        #         setattr(self, item.lower(), self.lldp[item])

        # if 'chassisid' in self.lldp:
        #     self.lldp_chassis_id = self.lldp['chassisid']
        # if 'portid' in self.lldp:
        #     self.lldp_port_id = self.lldp['portid']
        # if 'portdescr' in self.lldp:
        #     self.lldp_port_desc = self.lldp['portdescr']
        # if 'sysname' in self.lldp:
        #     self.lldp_sys_name = self.lldp['sysname']

    def get_netlinx_populate(self):
        # in auto  ip_address is disabled
        # in listen ip_address is disabled and system number is disabled
        # in url system number is disabled

        # in auto and listen we get the ip address of the master
        # in url we get the system number
        # Status, Master ip, Master number, Enabled
        # 'Online\n192.168.7.66\n0\n1\n'
        # Online\n192.168.7.34\n932\n1\n
        try:
            self.netlinx_status, self.netlinx_ip, self.netlinx_system, enable, extra = self.raw_netlinx.split('\n')
            if enable == '1':
                self.netlinx_enable = "True"
            elif enable == '0':
                self.netlinx_enable = "False"
            else:
                self.netlinx_enable = "Unknown"
        except Exception as error:
            logging.critical(f'unable to parse netlinx in datastore: {repr(self.raw_netlinx)}')
            logging.critical(error)

    def get_netlinx_enable(self):
        if self.netlinx_enable == "True":
            return True
        elif self.netlinx_enable == "False":
            return False
        else:
            return False

    def __str__(self):
        return f'SVSIUnit {self.ip_address}'

    def __repr__(self):
        return f'SVSIUnit {self.ip_address}'


@dataclass
class Preferences:
    main_list: list = field(default_factory=list)
    multicast_listen: bool = True
    check_for_updates: bool = True
    cols_selected: list = field(default_factory=lambda: ['Time', 'Name', 'Type', 'Model', 'MAC', 'IP', 'Serial',
                                                         'Stream', 'Mode', 'Resolution', 'Audio', 'Status'])
    svsi_username: str = 'admin'
    svsi_password: str = b'gAAAAABd-DMXaaHBEEutsj_ZYMYVRmneBMnOt8NfeBUhiBYS1LbNeaS7Pt9rV65ZTkQcOSHyTnK7BfiLQiwKr8T1zO49IgwyNw=='  # password

    firmware_directory: str = os.path.expanduser(os.path.join('~', 'Documents', 'Magic Svsi Configurator', 'firmware'))
    tmp_directory: str = os.path.expanduser(os.path.join('~', 'Documents', 'Magic Svsi Configurator', 'tmp'))
    # Recently used values
    start_ip: str = ''
    end_ip: str = ''
    master_mode: str = 'URL'
    # netlinx_ip: str = ''
    # netlinx_port: str = '1319'
    last_netlinx_device: str = '0'
    last_master_ip: str = ''
    # netlinx_system: str = '1'
    netlinx_username: str = ''
    netlinx_password: str = ''

    # AES 67
    last_aes_67_address: str = ''
    last_aes_67_port: str = ''

    def __post__init(self):
        for directory in [self.firmware_directory, self.tmp_directory]:
            if not os.path.exists(self.firmware_directory):
                try:
                    os.makedirs(directory)
                except Exception as error:
                    print(f"Unable to create folder {directory} ", error)

    def set_svsi_password(self, password):
        self.svsi_password = cipher_suite.encrypt(password.encode())

    def get_svsi_password(self):
        try:
            return cipher_suite.decrypt(self.svsi_password).decode()
        except Exception:
            return ''

    def set_password(self, password):
        self.netlinx_password = cipher_suite.encrypt(password.encode())

    def get_password(self):
        try:
            return cipher_suite.decrypt(self.netlinx_password).decode()
        except Exception:
            return ''
        
    


@dataclass
class InterfaceConfig:
    index: int = 0
    interface_index: int = 0
    ip_enabled: bool = False
    description: str = ''
    ip_addresses: list = field(default_factory=list)
    subnet_addresses: list = field(default_factory=list)
    gateway_addresses: list = field(default_factory=list)
    dhcp_enabled: bool = True
    broadcast_addresses: list = field(default_factory=list)

    def populate_broadcast_addresses(self):
        for i, ip in enumerate(self.ip_addresses):
            try:
                my_net = ipaddress.IPv4Network(ip + '/' + self.subnet_addresses[i], strict=False)
            except Exception:
                # Not IPv4
                self.broadcast_addresses.append(None)
                continue
            self.broadcast_addresses.append(str(my_net.broadcast_address))
