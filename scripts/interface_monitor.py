from threading import Thread
import time
import wmi
import pythoncom
import queue
from pydispatch import dispatcher
try:
    from scripts import datastore
except Exception:
    pass


class InterfaceMonitor(Thread):
    """The ip monitor thread"""

    def __init__(self, my_queue):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.my_queue = my_queue
        self.interface_list = []
        Thread.__init__(self)

    def run(self):
        """Run Worker Thread."""
        pythoncom.CoInitialize()
        self.interface_list = self.get_current_interfaces()
        while True:
            # gets the job from the queue
            job = self.my_queue.get()
            # print 'job: ', job
            getattr(self, job[0])(job)
            # send a signal to the queue that the job is done
            self.my_queue.task_done()

    def check_changes(self, command):
        # check if configs have changed (polling)
        # only send a update if necessary
        current_interfaces = self.get_current_interfaces()

        if self.interface_list != current_interfaces:
            self.interface_list = current_interfaces
            self.send_update()
        # else:
        #     print('no change')

    def get_current_interfaces(self):
        """Get the nic's"""
        interfaces = []
        for interface in wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True):
            interfaces.append(datastore.InterfaceConfig(index=interface.Index,
                                                        description=interface.Description,
                                                        ip_addresses=list(interface.IPAddress),
                                                        subnet_addresses=list(interface.IPSubnet),
                                                        gateway_addresses=interface.DefaultIPGateway,
                                                        dhcp_enabled=interface.DHCPEnabled))
        for interface in interfaces:
            interface.populate_broadcast_addresses()
            # print(interface)

        return interfaces

    def send_update(self, job=None):
        # print('sending update')
        dispatcher.send(signal="Interface Update", sender=self, interface_list=self.interface_list)

    def send_error(self, info):
        dispatcher.send(signal="Alert", sender=self, caption='Error', message=info)


def incoming(sender, interface_list):
    print('in update')
    print(interface_list)
    import pickle
    with open('interface.pkl', 'wb') as f:
        pickle.dump(interface_list, f)


def main():
    """Launch the main program"""
    dispatcher.connect(incoming,
                       signal="IP Monitor",
                       sender=dispatcher.Any)
    dispatcher.connect(incoming,
                       signal="Interface Update",
                       sender=dispatcher.Any)
    control_queue = queue.Queue()
    my_monitor = InterfaceMonitor(control_queue)
    my_monitor.setDaemon(True)
    my_monitor.start()

    count = 0
    control_queue.put(['send_update'])
    while True:
        count += 1
        control_queue.put(['check_changes'])
        if count >= 2:
            break
        time.sleep(5)


if __name__ == '__main__':
    import datastore
    main()

