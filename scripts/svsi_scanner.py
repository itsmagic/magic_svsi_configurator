import socket
import binascii
import time
# import ipaddress

from threading import Thread
from pydispatch import dispatcher
import re
try:
    from scripts import datastore
except Exception:
    pass


class SVSIScanner(Thread):
    """The SVSI thread"""

    def __init__(self, interface, ip_address='255.255.255.255', timeout=15):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.interface = interface
        self.ip_address = ip_address
        self.timeout = timeout
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown Scan",
                           sender=dispatcher.Any)
        Thread.__init__(self)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        """Run Worker Thread."""
        data_packs = ['0114cafe1234ffffffff',

                      '0119cafe1234ffffffff',

                      '0196cafe1234ffffffff',

                      '0198cafe1234ffffffff',

                      '01b4cafe1234ffffffff',
                      '01b5cafe1234ffffffff',
                      '01b6cafe1234ffffffff',
                      '01b7cafe1234ffffffff',
                      '01b8cafe1234ffffffff',
                      '01b9cafe1234ffffffff',
                      '01bacafe1234ffffffff',

                      '01cacafe1234ffffffff',
                      '01cbcafe1234ffffffff',
                      '01cccafe1234ffffffff',
                      '01cdcafe1234ffffffff',
                      '01cecafe1234ffffffff']

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            if self.ip_address == "255.255.255.255":
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sock.bind((self.interface.ip_addresses[0], 0))
            sock.setblocking(0)

        except IOError:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            dispatcher.send(signal="SVSIMonitor", sender=self, data={'Error': 'Socket in use'})
            return
        try:
            for data in data_packs:
                if data == '01b7cafe1234ffffffff':
                    sock.sendto(binascii.unhexlify(data), (self.ip_address, 50006))
                else:
                    sock.sendto(binascii.unhexlify(data), (self.ip_address, 50001))
                time.sleep(.1)
        except IOError as error:
            if error.args[0] == 10051:
                print("Skipping this error: ", error)
            else:
                print('Sendto socket error: ', error)
                # dispatcher.send(signal="Error Message", sender=self, data={'message': 'Another program is using the SVSI multicast port.\rPlease check if N-Able is running,\ror use broadcast to discover units.', 'caption': 'Socket in use'})
            return
        # for data in data_pack_2:
        #     sock.sendto(binascii.unhexlify(data), ('255.255.255.255', 50001))
        #     # time.sleep(.02)
        dispatcher.send(signal="Scan Update", data={'completed': 0, 'started': 1})
        data = ""
        count = 0
        while not self.shutdown:
            count += 1
            if count > self.timeout:
                break
            try:
                # print('waiting')
                data, source = sock.recvfrom(4000)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    time.sleep(1)
                    continue
                else:
                    # Connection was probably closed
                    break
                    # print("Error listening: ", error)
            try:
                if data == '' or data[:4].decode() != 'SVSI':
                    # print('not equal: ', data)
                    continue
                svsi_type = data.decode().split(':')[1].replace('NAME', '')
                reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
                my_dict = {}
                for item in re.findall(reg_pat, data.decode()):
                    my_dict[item[0]] = item[1]
            except Exception as error:
                print('Error parsing packet: ', error)
                continue
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=svsi_type,
                                          # status="broadcast",
                                          get_status=my_dict)
            dispatcher.send(signal="Unit Detected", data=new_unit, multicast=False)

        if not self.shutdown:
            dispatcher.send(signal="Scan Update", data={'completed': 1, 'started': 0})


def incoming(sender, data):
    print(sender)
    print(data)


def new_unit(sender, data, multicast):
    print(data)
    print(multicast)


def main():
    dispatcher.connect(incoming, signal="SVSIMonitor", sender=dispatcher.Any)
    dispatcher.connect(new_unit, signal="Unit Detected")
    # my_interface = datastore.InterfaceConfig()
    # my_interface.ip_addresses = ['10.0.0.27']
    import pickle
    with open('interface.pkl', 'rb') as f:
        my_interfaces = pickle.load(f)

    # Broadcast scan
    for interface in my_interfaces:
        test = SVSIScanner(interface)
        test.setDaemon(True)
        test.start()

    # import time
    # ip_range = IPRange('192.168.50.1', '192.168.57.254')


    # # Get listen ips for units
    # # ip_subnets [{'192.168.0.0/24': {'units': [unit, unit...], 'listen_ip': '192.168.0.200',
    # #              '198.18.0.0/12':  {'units': [unit, unit...], 'listen_ip': '198.18.0.200'}]


    # start_time = time.time()
    # # count = 0
    # for ip_address in ip_range:
    #     if ip_address.version != 4:
    #         continue

    #     # for interface in my_interfaces:
    #     # count += 1
    #     # print('active: ', threading.active_count())

    #     # while count > (70 * (time.time() - start_time)):
    #     #     print('waiting')
    #     #     time.sleep(.1)
    #     # print(f'on interface: {my_interfaces[4].ip_addresses}')
    #     starting_thread = True
    #     while starting_thread:
    #         try:
    #             test = SVSIScanner(my_interfaces[4], ip_address=str(ip_address))
    #             # test = SVSIScanner(interface)
    #             test.setDaemon(True)
    #             test.start()
    #             starting_thread = False
    #         except RuntimeError as error:
    #             # print('my_error', error, dir(error), error.__dict__)
    #             time.sleep(15)

    time.sleep(10)
    # test.shutdown = True


if __name__ == '__main__':
    import datastore
    main()
