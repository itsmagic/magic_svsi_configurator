import socket
import re
import time
import logging
from threading import Thread
from pydispatch import dispatcher
from queue import Queue
from pprint import pprint

try:
    from scripts import datastore
except Exception:
    try:
        import datastore
    except Exception:
        pass
    pass

logger = logging.getLogger("APIActionsThread")

class APIActionsThread(Thread):

    def __init__(self, queue, parent, port=50001):
        Thread.__init__(self)
        self.parent = parent
        self.queue = queue
        self.port = port
        self.file_transfer_complete = False
        self.file_transfer_progress = ''
        self.shutdown = False
        dispatcher.connect(self.unit_upgrading, signal='Unit Upgrading')
        dispatcher.connect(self.unit_progress, signal='HTTP Progress')
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def unit_upgrading(self, sender, data):
        self.file_transfer_complete = True

    def unit_progress(self, sender, data):
        self.file_transfer_progress = data['message']

    def shutdown_signal(self, sender):
        """Shut down the thread"""
        self.shutdown = True

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def get_status(self, job):
        unit = job[1]

        try:
            unit.status = 'Getting Status'
            dispatcher.send(signal='Status Update', data=unit)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(b'getStatus')
            data = s.recv(4096)
            if data[:4].decode() != 'SVSI':
                print('not equal: ', data)
                s.close()
                return
            svsi_type = data.decode().split(':')[1].replace('NAME', '')
            reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
            my_dict = {}
            for item in re.findall(reg_pat, data.decode()):
                my_dict[item[0]] = item[1]
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=svsi_type,
                                          # status="broadcast",
                                          get_status=my_dict)
            dispatcher.send(signal="Unit Detected", data=new_unit, multicast=False)
            s.close()
        except Exception as error:
            unit.status = f'Unable to get status: {error}'
            dispatcher.send(signal='Status Update', data=unit)
            print("Error getting status", error)
            return
        unit.status = 'Success'

        dispatcher.send(signal='Status Update', data=unit)

    def rx_hdcp(self, job):
        unit = job[1]
        hdcp = job[2]
        modes = {'auto': 'rxforcecp:0',
                 'on': 'rxforcecp:1',
                 'off': 'rxforcecp:2'}
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(modes[hdcp].encode())
            s.close()
        except Exception as error:
            print("Error setting rx hdcp", error)
            return
        self.get_status(job)
        self.parent.main_list_queue_thread.put(['refresh_obj', unit])

    def tx_hdcp(self, job):
        unit = job[1]
        hdcp = job[2]
        modes = {'on': 'setSettings:contentProtection:on',
                 'off': 'setSettings:contentProtection:off'}
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(modes[hdcp].encode())
            s.close()
        except Exception as error:
            print("Error setting tx hdcp", error)
            return
        self.get_status(job)
        self.parent.main_list_queue_thread.put(['refresh_obj', unit])

    def lldp(self, job):
        unit = job[1]
        try:
            unit.status = 'Getting LLDP'
            dispatcher.send(signal='Status Update', data=unit)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(b'getNetStatus')
            data = s.recv(4096)
            reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
            my_dict = {}
            for item in re.findall(reg_pat, data.decode()):
                my_dict[item[0].lower()] = item[1]
            s.close()
        except Exception as error:
            unit.status = f'Error getting LLDP: {error}'
            dispatcher.send(signal='Status Update', data=unit)
            print("Error getting lldp", error)
            return
        unit.lldp = my_dict
        unit.get_lldp_populate()
        unit.status = 'Success'
        dispatcher.send(signal='Status Update', data=unit)

    def monitor_unit_for_reboot(self, job):
        """Opens a connect to a unit, and sends a message when this connection drops"""
        unit = job[1]
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # s.settimeout(2)
            s.connect((unit.ip_address, self.port))
            count = 0
            while not self.shutdown:
                count += 1
                unit.status = f'Waiting for reboot {count}'
                dispatcher.send(signal='Status Update', data=unit)
                s.send(b'getStatus')
                # s.recv(4096)
                time.sleep(1)
            s.close()
        except:
            try:
                s.close()
            except:
                pass
            unit.status = f'Rebooting'
            unit.upgrade_status = 'Rebooting'
            dispatcher.send(signal='Status Update', data=unit)

    def monitor_unit_for_update(self, job):
        # {'firmware': '7/11/2017', 'count': 35, 'unit': SVSIUnit 10.0.0.29}}
        unit = job[1]
        firmware = job[2]
        print(f'{unit} upgrade status: {unit.upgrade_status}')
        count = 0
        # if unit is not in units upgrading, we don't need to monitor any more...
        while not self.shutdown and unit in self.parent.units_upgrading:
            # print('in monitor')
            time.sleep(1)
            if "Rebooting" in unit.upgrade_status:
                # attempt connection
                # We are now waiting for the unit to boot and then verify firmware, we can start counting
                count += 1
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    s.settimeout(1)
                    s.connect((unit.ip_address, self.port))
                    s.send(b'getStatus')
                    data = s.recv(4096)
                    reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
                    my_dict = {}
                    for item in re.findall(reg_pat, data.decode()):
                        my_dict[item[0]] = item[1]
                    # pprint(my_dict)
                    if 'UPDATE' in my_dict:
                        if my_dict['UPDATE'] == '2':
                            # Need to verify update
                            # print('need to verify')
                            if firmware != my_dict['SWVER']:
                                unit.status = "Unable to verify upgrade"
                                dispatcher.send(signal='Status Update', data=unit)
                            else:
                                # print('sending upgrade complete')
                                unit.get_status = my_dict
                                unit.get_status_populate()
                                unit.status = "Upgrade Complete"
                                dispatcher.send(signal='Status Update', data=unit)
                                dispatcher.send(signal='Upgrade Complete', data=unit)
                            s.send(b'setSettings:confirmUpdate\r')
                            s.recv(4096)
                            s.send(b'getStatus')
                            s.recv(4096)
                            s.close()
                            return
                        if my_dict['UPDATE'] == '0':
                            # print('Got a 0 in update?')
                            unit.status = "Unable to verify upgrade"
                            dispatcher.send(signal='Status Update', data=unit)
                            dispatcher.send(signal='Upgrade Complete', data=unit)
                            s.close()
                            return
                    else:
                        logging.debug(f"couldn't find update")
                    s.close()

                except Exception as error:
                    # print('Unable to connect: ', error)
                    try:
                        s.close()
                    except:
                        pass
                    unit.status = f'Rebooting {count}'
                    dispatcher.send(signal='Status Update', data=unit)

    def check_unit(self, job):
        # {'firmware': '7/11/2017', 'count': 35, 'unit': SVSIUnit 10.0.0.29}}
        unit = job[1]['unit']
        firmware = job[1]['firmware']
        count = job[1]['count']
        count += 1
        # print(f'{unit} upgrade status: {unit.upgrade_status}')
        if "Rebooting" in unit.upgrade_status:
            # attempt connection
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(1)
                s.connect((unit.ip_address, self.port))
                s.send(b'getStatus')
                data = s.recv(4096)
                reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
                my_dict = {}
                for item in re.findall(reg_pat, data.decode()):
                    my_dict[item[0]] = item[1]
                # pprint(my_dict)
                if 'UPDATE' in my_dict:
                    if my_dict['UPDATE'] == '2':
                        # Need to verify update
                        # print('need to verify')
                        if firmware != my_dict['SWVER']:
                            unit.status = "Unable to verify upgrade"
                            dispatcher.send(signal='Status Update', data=unit)
                        else:
                            # print('sending upgrade complete')
                            unit.status = "Upgrade Complete"
                            dispatcher.send(signal='Status Update', data=unit)
                            dispatcher.send(signal='Upgrade Complete', data=unit)
                        s.send(b'setSettings:confirmUpdate')
                        s.close()
                        return
                    if my_dict['UPDATE'] == '0':
                        # print('Got a 0 in update?')
                        unit.status = "Unable to verify upgrade"
                        dispatcher.send(signal='Status Update', data=unit)
                        dispatcher.send(signal='Upgrade Complete', data=unit)
                else:
                    logging.debug("couldn't find update")
                s.close()

            except Exception as error:
                print('Unable to connect: ', error)
                try:
                    s.close()
                except:
                    pass
                unit.status = f'Rebooting {count}'
                dispatcher.send(signal='Status Update', data=unit)

    def check_for_firmware_update(self, job):
        unit = job[1]
        firmware_dict = job[2]
        unit.status = 'Checking Firmware Version'
        dispatcher.send(signal='Status Update', data=unit)
        self.get_status(job)
        unit.status = self.parent.check_for_firmware_update_on_unit(unit, firmware_dict)['status']
        dispatcher.send(signal='Status Update', data=unit)

    def tx_control(self, job):
        unit = job[1]
        enable = job[2]
        if enable:
            unit.status = "Attempting to Enable TX"
        else:
            unit.status = "Attempting to Disable TX"
        dispatcher.send(signal='Status Update', data=unit)
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            if enable:
                s.send(b'txenable\r')
                s.recv(4096)
                unit.status = "TX Enabled"
            else:
                s.send(b'txdisable\r')
                s.recv(4096)
                unit.status = "TX Disabled"
            s.close()
        except Exception as error:
            print('Unable to txenable /txdisable: ', error)
            unit.status = "Unable to txenable / txdisable " + str(error)
        dispatcher.send(signal='Status Update', data=unit)

    def aes67_control(self, job):
        unit = job[1]
        data = job[2]
        enable = data['enable']  # True or False
        address = data['address']
        port = data['port']
        channels = data['channels']  # 2 or 8
        if enable:
            unit.status = "Configuring AES67"
        else:
            unit.status = "Attempting to Disable AES67"
        dispatcher.send(signal='Status Update', data=unit)
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            if unit.box_type == 'TX':
                if enable:
                    s.send(f"aes67MCIPAddr:{address}\raes67Port:{port}\raes67Audio:on\r".encode())
                    s.recv(4096)
                    unit.status = "AES67 Configured"
            else:  # RX
                if enable:
                    s.send(f"aes67MCIPAddr:{address}\raes67Port:{port}\raes67Channels:{channels}\raes67Audio:on\r".encode())
                    s.recv(4096)
                    unit.status = "AES67 Configured"

            if not enable:
                s.send(f"aes67Audio:off\r".encode())
                s.recv(4096)
                unit.status = "AES67 Disabled"
            s.close()
        except Exception as error:
            print('Unable to configure AES67: ', error)
            unit.status = "Unable to configure AES67 " + str(error)
        dispatcher.send(signal='Status Update', data=unit)
        self.get_status(job)
        self.parent.main_list_queue_thread.put(['refresh_obj', unit])

    def send_command(self, job):
        unit = job[1]
        command = job[2]
        unit.status = f"Sending command: {command}\r"
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(f"{command}\r".encode())
            print(s.recv(4096))
            s.close()
        except Exception as error:
            print('Unable to send command: ', error)
            unit.status = "Unable to send command " + str(error)
        dispatcher.send(signal='Status Update', data=unit)


def status(status):
    pprint(status)


def main():
    import datastore
    dispatcher.connect(status, signal="webAPI")
    api_queue = Queue()
    api_job_thread = APIActionsThread(api_queue)
    api_job_thread.setDaemon(True)
    api_job_thread.start()
    unit = datastore.SVSIUnit()
    unit.ip_address = "192.168.7.116"
    api_queue.put(['lldp', unit])
    api_job_thread.shutdown = True
    api_job_thread.join()


if __name__ == '__main__':
    main()
