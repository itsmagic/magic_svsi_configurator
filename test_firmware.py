import magic_svsi_configurator
import os

def test_firmware_parse():
    firmware_directory = os.path.expanduser(os.path.join('~', 'Documents', 'Magic Svsi Configurator', 'firmware'))
    my_firmware = magic_svsi_configurator.Magic_Svsi_Configurator_Frame.parse_firmware_folder(None, firmware_directory)

    # Test top level
    assert 'KVMUpdate_06-06-2018_v1.15.18' in my_firmware
    assert my_firmware['KVMUpdate_06-06-2018_v1.15.18']['WEBVER'] == '1528294497'
    assert my_firmware['KVMUpdate_06-06-2018_v1.15.18']['SWVER'] == '6/6/2018'
    assert my_firmware['KVMUpdate_06-06-2018_v1.15.18']['MINSWVER'] == '6/3/13'
    assert my_firmware['KVMUpdate_06-06-2018_v1.15.18']['INCLUDE'] == ['N1115']
    assert my_firmware['KVMUpdate_06-06-2018_v1.15.18']['path'] == 'C:\\Users\\jimm\\Documents\\Magic Svsi Configurator\\firmware' 

    # Test in a folder
    assert 'N4Update_2023-02-09' in my_firmware
    assert my_firmware['N4Update_2023-02-09']['WEBVER'] == '1675922400'
    assert my_firmware['N4Update_2023-02-09']['SWVER'] == '2/9/2023'
    assert my_firmware['N4Update_2023-02-09']['MINSWVER'] == '04/20/2013'
    assert my_firmware['N4Update_2023-02-09']['GITVER'] == '1.0.6'
    assert my_firmware['N4Update_2023-02-09']['INCLUDE'] == ['N432A']
    assert my_firmware['N4Update_2023-02-09']['path'] == 'C:\\Users\\jimm\\Documents\\Magic Svsi Configurator\\firmware\\Test'

def test_served_file_is_correct_firmware():
    firmware_directory = os.path.expanduser(os.path.join('~', 'Documents', 'Magic Svsi Configurator', 'firmware'))
    firmware_dict = magic_svsi_configurator.Magic_Svsi_Configurator_Frame.parse_firmware_folder(None, firmware_directory)
    firmware_name = 'N4Update_2023-02-09'
    assert os.path.join(firmware_dict[firmware_name]['path'], firmware_name + '.bin') == "C:\\Users\\jimm\\Documents\\Magic Svsi Configurator\\firmware\\Test\\N4Update_2023-02-09.bin"
    firmware_name = 'KVMUpdate_06-06-2018_v1.15.18'
    assert os.path.join(firmware_dict[firmware_name]['path'], firmware_name + '.bin') == "C:\\Users\\jimm\\Documents\\Magic Svsi Configurator\\firmware\\KVMUpdate_06-06-2018_v1.15.18.bin"

